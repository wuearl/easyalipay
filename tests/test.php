<?php
require __DIR__.'/../vendor/autoload.php';
use EasyAlipay\Factory;

$options = [
    'app_id'                   => '2018070360570197',
    'gateway_url'              => "https://openapi.alipaydev.com/gateway.do",//示例中使用的是沙箱环境网关，线上gateway_url：https://openapi.alipay.com/gateway.do
    'sign_type'                => "RSA2",
    'encrypt_key'              =>'oZXL/lMFlvG818o1jbgM2w==',
    'response_type'            => 'collection',
    'alipay_public_key'        => 'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsDos+q5ZZa2JWtNLNluV8R6WqJSPUcJ39HpflCb8AEpSLTcjxsWG0RG3nvv7xBAHEOSRET8dTKPoWZXy1TAstv7WY4epiWZwogRzpNEPRbKyEBmbzkiOENW/27IdD6GFQ6e+MPE/BOPqd+hjR5SFghrQNpwZECWuCDFpsJq9G7EtTBUB1av52o/wYqWU1eCeUq+3MrtAD7p0Sh2V/B9jdmkpZhwGI/yHL23MHJI5J2aCNCfEQF520IHwHELrIq7xRv2hVE53APB/dueTTsIKNIWnbA8QbK3MUOgykG80UVxtZMl14Y3eB7RyzY70I5NimszSTSG6OQ2P78PMZjd77wIDAQAB',
    'merchant_private_key'     => 'MIIEogIBAAKCAQEAi6lTghhlFBWvrp3hjxzdr4+tUaeDbAilxjOsyCWCfmDdqP7vypN1ZFphJwRx41639/VEpMxe7yMoUwVJPR7VpmWCm1V/sfExU+9pBAtVrGRUZLwS+dkB9vlEH9dYRjeMOmw4hXvf9X9JlQEpH4HkHpE5BjVk1OexY9zwzfoa0lV8uDId/vxYoRpSU9Hm8CdgZMySizrEicD+5isBiad8uKIZ3fPRb9v60L7xL1LuaCjyY2t8BehscI4cND1D5BQZyqkXY19a4qD5zYjRRK26jOYxu5X/jIREK9SYjb5n3b6G8/NU1zMO5vQA8wkqjMLi1xj+Zla2Qlx3Qth5docPwQIDAQABAoIBAHhmJxm2Uh04fNW6QeOQbQSWhkIoeUG/SrpNfhylSZQNicnQzYQED2XODIcIa+4pj8txZBX2Ibliw7aejf7lACaMyEWFNap/VZkYgZ3874TIkYCuKmW90NXLGhGhIfaWmIsdFLTuj6oia5rK/1qsLa60X0ac7Sol+2Ut6WKIxGqiBcZvziGQJoN0uI7l0gHX+XFUpyss0nKJpEinjUAbyS5QGwT8mBTsJLyVAiD+dZV2ig1DRBo8McAE/XebCTg2e6PfckcIjNXAfanLFSBLRLwdE+6Obx5fJoSPlTwnMK/Ln+PeuGgDJSfMkEunT5g0IxTEnVdEBvVgGyJK1J2gkckCgYEAw75ye9uRoDzkleM4T5Bo5bbCQnG9GUYkLeLGz/K073f8Zd2lDgp5+ODtRH3ftAdDv/g3SdJOU34FhI6vGJRbi+wLEP4GQq78t8Ez3623CotskPVBDOtRMOKuGeybB1OQYFwOVkwnFVyV+6GG/uNuybIuIxyNuiOLb6W2C9fyEx8CgYEAtqdPnwPFKU8gH12DW4KbT9GHFsgRA510mnv3MXSK2v9NcI4LFNc/zI6HFsIICXOgXwiAMfWKzOeqIAWmPB3DESvSqG+l6CubQLSmp21LJKh3Y7JbX9gwKY2UP6p/Yev86Ga6m9xdUkrFAQNI5oJs56asHjavSIJJyDsCJ0IIYR8CgYB7snNYcAXT9Acj7UYdiY2wdNDH+mZcfWXJlYC70o2isvlOSkXO3LH9o+5slPr0Io8e1jXiMcOLoZzNCvn9l2tOvei0YUMFit1rJe2pEXcVc8w0wrfL+T0Cno0VFt9VPuqC6kmpIVClgC/Lp2TO2FyfmzjquVWa5nFsKZUkYN+6UwKBgGM/AfprOAQ8JD4mt6tPikiSlw5/4w7NzX/rf+N1acWZF2DjLY5Dbz0c7LYm8+r+0tMQcWez/Zlc/4mqyCq+GTIJV8uB7un6V7+O6UbsEfp3N3gKf6/SbkkFztnRMKnqal91AkySLnr6eZUVTdVCZR5x5+/60r9ZW/Habk8aiCcNAoGAGPCxjwznnHxe0C/325N8JfGuamOayyOOfGGZY5zay3m8MIbFdOKykd4aqHiSE15kmJDsmKoFedlOZYXKqm+bA+kii2AKokrnd1H+I2xnqZnZWMB64bHwPZQA9fIPpvyvHgXYY9bxp4oAMHPwrvbY2nRpplO3Jz7Oax2LFBBLBQY=',
    // ...
];

//本测试用例提供的是沙箱环境的配置信息，线上调用请更改为自己应用对应的信息
//app_id，alipay_public_key（支付宝公钥），merchant_private_key（应用私钥）为必填项
//gateway_url不设置就默认线上网关
//sign_type 不设置就默认RSA2（目前线上环境支持RSA）
//charset  不设置就默认UTF-8


//接口测试用例
$app = Factory::payment($options);

var_dump($app['query']->query("1561946980099_demo_pay"));
