<?php

namespace EasyAlipay\OpenPublic\Message;

use EasyAlipay\Kernel\AppClient;
use EasyAlipay\Kernel\AppRequest;
use EasyAlipay\OpenPublic\Model\AlipayOpenPublicMessageContentCreateContentBuilder;
use EasyAlipay\OpenPublic\Model\AlipayOpenPublicMessageContentModifyContentBuilder;
use EasyAlipay\OpenPublic\Model\AlipayOpenPublicMessageTotalSendContentBuilder;
use EasyAlipay\OpenPublic\Model\AlipayOpenPublicMessageQueryContentBuilder;
use EasyAlipay\OpenPublic\Model\AlipayOpenPublicLifeMsgRecallContentBuilder;

class Client extends AppClient
{
    /**
     * 创建图文消息
     * @param string $title
     * @param string $cover
     * @param string $content
     * @param string $could_comment
     * @param string $ctype
     * @param string $benefit
     * @param string $ext_tags
     * @param string $login_ids
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function createImageTextContent(string $title,string $cover,string $content,string $could_comment,string $ctype,string $benefit,string $ext_tags,string $login_ids)
    {
        //构造查询业务请求参数对象
        $createContentBuilder = new AlipayOpenPublicMessageContentCreateContentBuilder();
        $createContentBuilder->setTitle($title);
        $createContentBuilder->setCover($cover);
        $createContentBuilder->setContent($content);
        $createContentBuilder->setCouldComment($could_comment);
        $createContentBuilder->setCtype($ctype);
        $createContentBuilder->setBenefit($benefit);
        $createContentBuilder->setExtTags($ext_tags);
        $createContentBuilder->setLoginIds($login_ids);
        $request = new AppRequest ();
        $request->setBizContent($createContentBuilder->getBizContent());
        $request->setApiMethodName("alipay.open.public.message.content.create");
        return($this->execute($request));
    }

    /**
     * 更新图文消息
     * @param string $content_id
     * @param string $title
     * @param string $cover
     * @param string $content
     * @param string $could_comment
     * @param string $ctype
     * @param string $benefit
     * @param string $ext_tags
     * @param string $login_ids
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function modifyImageTextContent(string $content_id,string $title,string $cover,string $content,string $could_comment,string $ctype,string $benefit,string $ext_tags,string $login_ids)
    {
        //构造查询业务请求参数对象
        $modifyContentBuilder = new AlipayOpenPublicMessageContentModifyContentBuilder();
        $modifyContentBuilder->setContentId($content_id);
        $modifyContentBuilder->setCover($cover);
        $modifyContentBuilder->setContent($content);
        $modifyContentBuilder->setCouldComment($could_comment);
        $modifyContentBuilder->setCtype($ctype);
        $modifyContentBuilder->setBenefit($benefit);
        $modifyContentBuilder->setExtTags($ext_tags);
        $modifyContentBuilder->setLoginIds($login_ids);
        $request = new AppRequest ();
        $request->setBizContent($modifyContentBuilder->getBizContent());
        $request->setApiMethodName("alipay.open.public.message.content.modify");
        return($this->execute($request));
    }

    /**
     * 群发消息(文本)
     * @param string $text
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function sendText(string $text)
    {
        //构造查询业务请求参数对象
        $totalSendContentBuilder = new AlipayOpenPublicMessageTotalSendContentBuilder();
        $totalSendContentBuilder->setText($text);
        $totalSendContentBuilder->setMsgType("text");
        $request = new AppRequest ();
        $request->setBizContent($totalSendContentBuilder->getBizContent());
        $request->setApiMethodName("alipay.open.public.message.total.send");
        return($this->execute($request)) ;
    }

    /**
     * 群发消息(图文)
     * @param string $articles
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function sendImageText(string $articles)
    {
        //构造查询业务请求参数对象
        $totalSendContentBuilder = new AlipayOpenPublicMessageTotalSendContentBuilder();
        $totalSendContentBuilder->setArticles($articles);
        $totalSendContentBuilder->setMsgType("image-text");
        $request = new AppRequest ();
        $request->setBizContent($totalSendContentBuilder->getBizContent());
        $request->setApiMethodName("alipay.open.public.message.total.send");
        return($this->execute($request)) ;
    }

    /**
     * 查询已发送消息
     * @param string $message_ids
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function query(string $message_ids)
    {
        //构造查询业务请求参数对象
        $queryContentBuilder = new AlipayOpenPublicMessageQueryContentBuilder();
        $queryContentBuilder->setMessageIds($message_ids);
        $request = new AppRequest ();
        $request->setBizContent($queryContentBuilder->getBizContent());
        $request->setApiMethodName("alipay.open.public.message.query");
        return($this->execute($request)) ;
    }

    /**
     * 消息撤回
     * @param string $message_id
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function recall(string $message_id)
    {
        //构造查询业务请求参数对象
        $recallContentBuilder = new AlipayOpenPublicLifeMsgRecallContentBuilder();
        $recallContentBuilder->setMessageId($message_id);
        $request = new AppRequest ();
        $request->setBizContent($recallContentBuilder->getBizContent());
        $request->setApiMethodName("alipay.open.public.life.msg.recall");
        return($this->execute($request)) ;
    }
}
