<?php

namespace EasyAlipay\OpenPublic\Auth;

use EasyAlipay\Kernel\AccessToken as BaseAccessToken;

class AccessToken extends BaseAccessToken
{
    protected $endpointToGetToken = 'https://api.weixin.qq.com/cgi-bin/token';

    protected function getCredentials(): array
    {
        return [
            'grant_type' => 'authorization_code',
            'app_id' => $this->app['config']['app_id'],
            'secret' => $this->app['config']['secret'],
        ];
    }
}
