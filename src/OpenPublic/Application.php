<?php

namespace EasyAlipay\OpenPublic;

use EasyAlipay\Kernel\ServiceContainer;

/**
 * Class Application.
 *
 * @property \EasyAlipay\OpenPublic\Message\Client          $message
 * @property \EasyAlipay\OpenPublic\Template\Client         $template
 *
 */
class Application extends ServiceContainer
{
    /**
     * @var array
     */
    protected $providers = [
        Message\ServiceProvider::class,
        Template\ServiceProvider::class,
    ];

    public function redirect(string $redirect_uri, string $state, string $scope = 'auth_user')
    {
        $base = [
            'app_id' => $this->config['app_id'],
            'scope' => $scope,
            'redirect_uri' => $redirect_uri,
            'state' => $state
        ];
        $base_url = 'https://openauth.alipay.com/oauth2/publicAppAuthorize.htm';
        $query = http_build_query($base);
        return $base_url . '?' . $query;
    }
}
