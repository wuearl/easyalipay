<?php

namespace EasyAlipay\OpenPublic\Template;

use EasyAlipay\Kernel\AppClient;
use EasyAlipay\Kernel\AppRequest;
use EasyAlipay\OpenPublic\Model\AlipayOpenPublicTemplateMessageGetContentBuilder;
use EasyAlipay\OpenPublic\Model\AlipayOpenPublicTemplateMessageIndustryModifyContentBuilder;

class Client extends AppClient
{

    /**
     * 模板消息行业设置
     * @param string $primary_industry_name
     * @param string $primary_industry_code
     * @param string $secondary_industry_code
     * @param string $secondary_industry_name
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function setIndustry(string $primary_industry_name, string $primary_industry_code, string $secondary_industry_code, string $secondary_industry_name)
    {
        //构造查询业务请求参数对象
        $industryModifyContentBuilder = new AlipayOpenPublicTemplateMessageIndustryModifyContentBuilder();
        $industryModifyContentBuilder->setPrimaryIndustryName($primary_industry_name);
        $industryModifyContentBuilder->setPrimaryIndustryCode($primary_industry_code);
        $industryModifyContentBuilder->setSecondaryIndustryCode($secondary_industry_code);
        $industryModifyContentBuilder->setSecondaryIndustryName($secondary_industry_name);
        $request = new AppRequest ();
        $request->setBizContent($industryModifyContentBuilder->getBizContent());
        $request->setApiMethodName("alipay.open.public.template.message.industry.modify");
        return ($this->execute($request));
    }

    /**
     * 模板消息行业获取
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getIndustry()
    {
        //构造查询业务请求参数对象
        $request = new AppRequest ();
        $request->setApiMethodName("alipay.open.public.setting.category.query");
        return ($this->execute($request));
    }

    /**
     * 消息模板领取
     * @param string $template_id
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getTemplate(string $template_id)
    {
        //构造查询业务请求参数对象
        $getContentBuilder = new AlipayOpenPublicTemplateMessageGetContentBuilder();
        $getContentBuilder->setTemplateId($template_id);
        $request = new AppRequest ();
        $request->setBizContent($getContentBuilder->getBizContent());
        $request->setApiMethodName("alipay.open.public.template.message.get");
        return ($this->execute($request));
    }
}
