<?php

namespace EasyAlipay\Base;

use EasyAlipay\Kernel\ServiceContainer;

/**
 * Class Application.
 *
 * @property \EasyAlipay\Base\Oauth\Client            $oauth
 *
 */
class Application extends ServiceContainer
{
    /**
     * @var array
     */
    protected $providers = [
        Oauth\ServiceProvider::class,
    ];
}
