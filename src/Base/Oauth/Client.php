<?php

namespace EasyAlipay\Base\Oauth;

use EasyAlipay\Base\Model\AlipayUserInfoShareReqyest;
use EasyAlipay\Kernel\AppClient;
use EasyAlipay\Base\Model\AlipaySystemOauthTokenRequest;
use EasyAlipay\Kernel\Exceptions\InvalidSignException;
use function EasyAlipay\Kernel\decrypt;

class Client extends AppClient
{
    /**
     * 获取 token
     * @param string $grant_type
     * @param string $code
     * @param string $refresh_token
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getToken(string $grant_type, string $code, string $refresh_token = '')
    {
        $request = new AlipaySystemOauthTokenRequest();
        $request->setApiMethodName('alipay.system.oauth.token');
        $request->setGrantType($grant_type);
        $request->setCode($code);
        $request->setRefreshToken($refresh_token);
        return ($this->execute($request));
    }

    /**
     * @param string $token
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function user(string $token)
    {
        $this->config['auth_token'] = $token;
        $request = new AlipayUserInfoShareReqyest();
        $request->setApiMethodName('alipay.user.info.share');
        return ($this->execute($request));
    }

    /**
     * @param $content
     * @return false|string
     */
    public function decryptData($content)
    {
        $res = decrypt($content, $this->app->config->get('encrypt_key'));
        return $res;
    }
}
