<?php

namespace EasyAlipay\Base\Oauth;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ServiceProvider implements ServiceProviderInterface
{
    /**
     * @param Container $app
     */
    public function register(Container $app)
    {
        $app['oauth'] = function ($app) {
            return new Client($app);
        };
    }
}
