<?php

namespace EasyAlipay\Base\Model;

/* *
 * 功能：alipay.system.oauth.token（换取授权访问令牌接口）业务参数封装
 */

use EasyAlipay\Kernel\AppRequest;

class AlipaySystemOauthTokenRequest extends AppRequest
{
    /**
     * 授权码，用户对应用授权后得到。
     * @var string
     */
    protected $code;

    /**
     * 值为authorization_code时，代表用code换取；值为refresh_token时，代表用refresh_token换取
     * @var string
     */
    protected $grantType;

    /**
     * 刷新令牌，上次换取访问令牌时得到。见出参的refresh_token字段
     * @var string
     */
    protected $refreshToken;

    /**
     * 设置code
     * @param $code
     */
    public function setCode($code)
    {
        $this->code = $code;
        $this->apiParas["code"] = $code;
    }

    /**
     * 获取code
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * 设置类型
     * @param $grantType
     */
    public function setGrantType($grantType)
    {
        $this->grantType = $grantType;
        $this->apiParas["grant_type"] = $grantType;
    }

    /**
     * 获取类型
     * @return mixed
     */
    public function getGrantType()
    {
        return $this->grantType;
    }

    /**
     * 设置刷新token
     * @param $refreshToken
     */
    public function setRefreshToken($refreshToken)
    {
        $this->refreshToken = $refreshToken;
        $this->apiParas["refresh_token"] = $refreshToken;
    }

    /**
     * 获取刷新token
     * @return mixed
     */
    public function getRefreshToken()
    {
        return $this->refreshToken;
    }
}
