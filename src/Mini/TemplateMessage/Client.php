<?php

namespace EasyAlipay\Mini\TemplateMessage;

use EasyAlipay\Kernel\AppClient;
use EasyAlipay\Kernel\AppRequest;
use EasyAlipay\Mini\Model\AlipayOpenAppMiniTemplatemessageSendContentBuilder;

class Client  extends AppClient
{
    /**
     * 小程序发送模板消息
     * @param string $to_user_id
     * @param string $form_id
     * @param string $user_template_id
     * @param string $page
     * @param string $data
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function send(string $to_user_id,string $form_id,string $user_template_id,string $page,string $data)
    {
        //构造查询业务请求参数对象
        $sendContentBuilder = new AlipayOpenAppMiniTemplatemessageSendContentBuilder();
        $sendContentBuilder->setToUserId($to_user_id);
        $sendContentBuilder->setFormId($form_id);
        $sendContentBuilder->setUserTemplateId($user_template_id);
        $sendContentBuilder->setPage($page);
        $sendContentBuilder->setData($data);
        $request = new AppRequest ();
        $request->setBizContent($sendContentBuilder->getBizContent());
        $request->setApiMethodName("alipay.open.app.mini.templatemessage.send");
        return($this->execute($request)) ;
    }

}
