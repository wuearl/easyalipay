<?php

namespace EasyAlipay\Mini\Risk;

use EasyAlipay\Kernel\AppClient;
use EasyAlipay\Kernel\AppRequest;
use EasyAlipay\Mini\Model\AlipaySecurityRiskContentDetectContentBuilder;

class Client extends AppClient
{
    /**
     * 小程序内容风险检测服务
     * @param string $content
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function detectContent(string $content)
    {
        //构造查询业务请求参数对象
        $riskContentBuilder = new AlipaySecurityRiskContentDetectContentBuilder();
        $riskContentBuilder->setContent($content);
        $request = new AppRequest ();
        $request->setBizContent($riskContentBuilder->getBizContent());
        $request->setApiMethodName("alipay.security.risk.content.detect");
        return($this->execute($request)) ;
    }

}
