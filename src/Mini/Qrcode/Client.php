<?php

namespace EasyAlipay\Mini\Qrcode;

use EasyAlipay\Kernel\AppClient;
use EasyAlipay\Kernel\AppRequest;
use EasyAlipay\Mini\Model\AlipayOpenAppQrcodeCreateContentBuilder;

class Client extends AppClient
{
    /**
     * 小程序生成推广二维码
     * @param string $url_param
     * @param string $query_param
     * @param string $describe
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function create(string $url_param,string $query_param,string $describe)
    {
        //构造查询业务请求参数对象
        $qrcodeContentBuilder = new AlipayOpenAppQrcodeCreateContentBuilder();
        $qrcodeContentBuilder->setUrlParam($url_param);
        $qrcodeContentBuilder->setQueryParam($query_param);
        $qrcodeContentBuilder->setDescribe($describe);
        $request = new AppRequest ();
        $request->setBizContent($qrcodeContentBuilder->getBizContent());
        $request->setApiMethodName("alipay.open.app.qrcode.create");
        return($this->execute($request)) ;
    }

}
