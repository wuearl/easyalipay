<?php

namespace EasyAlipay\Mini\Identification;

use EasyAlipay\Kernel\AppClient;
use EasyAlipay\Kernel\AppRequest;
use EasyAlipay\Mini\Model\ZolozIdentificationCustomerCertifyzhubQueryContentBuilder;
use EasyAlipay\Mini\Model\ZolozIdentificationUserWebQueryContentBuilder;

class Client extends AppClient
{
    /**
     * 人脸结果查询接口
     * @param string $biz_id
     * @param string $zim_id
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function queryCertifyzhub(string $biz_id,string $zim_id)
    {
        //构造查询业务请求参数对象
        $zhubQueryContentBuilder = new ZolozIdentificationCustomerCertifyzhubQueryContentBuilder();
        $zhubQueryContentBuilder->setBizId($biz_id);
        $zhubQueryContentBuilder->setZimId($zim_id);
        $zhubQueryContentBuilder->setFaceType(2);
        $request = new AppRequest ();
        $request->setBizContent($zhubQueryContentBuilder->getBizContent());
        $request->setApiMethodName("zoloz.identification.customer.certifyzhub.query");
        return($this->execute($request)) ;
    }

    /**
     * 人脸采集结果查询
     * @param string $biz_id
     * @param string $zim_id
     * @param string $extern_param
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function queryUserWeb(string $biz_id,string $zim_id,string $extern_param)
    {
        //构造查询业务请求参数对象
        $userWebQueryContentBuilder = new ZolozIdentificationUserWebQueryContentBuilder();
        $userWebQueryContentBuilder->setBizId($biz_id);
        $userWebQueryContentBuilder->setZimId($zim_id);
        $userWebQueryContentBuilder->setExternParam($extern_param);
        $request = new AppRequest ();
        $request->setBizContent($userWebQueryContentBuilder->getBizContent());
        $request->setApiMethodName("zoloz.identification.user.web.query");
        return($this->execute($request)) ;
    }

}
