<?php

namespace EasyAlipay\Kernel;

use EasyAlipay\Kernel\Contracts\Arrayable;
use EasyAlipay\Kernel\Exceptions\RuntimeException;
use EasyAlipay\Kernel\Support\Arr;
use EasyAlipay\Kernel\Support\Collection;

/**
 * @param $str
 * @param $screct_key
 * @return string
 */
function encrypt($str, $screct_key)
{
    $screct_key = base64_decode($screct_key);
    $str = trim($str);
    $iv_size = openssl_cipher_iv_length('AES-128-CBC');
    $iv = str_repeat("\0", $iv_size);
    $encrypt_str = openssl_encrypt($str,"AES-128-CBC",$screct_key,OPENSSL_RAW_DATA,$iv);
    return base64_encode($encrypt_str);
}

/**
 * @param $str
 * @param $screct_key
 * @return false|string
 */
function decrypt($str, $screct_key)
{
    $str = base64_decode($str);
    $screct_key = base64_decode($screct_key);
    $iv_size = openssl_cipher_iv_length('AES-128-CBC');
    $iv = str_repeat("\0", $iv_size);
    return openssl_decrypt($str,"AES-128-CBC",$screct_key,OPENSSL_RAW_DATA,$iv);
}

/**
 * @param $data
 * @param $key
 * @param null $default
 * @return mixed|null
 * @throws RuntimeException
 */
function data_get($data, $key, $default = null)
{
    switch (true) {
        case is_array($data):
            return Arr::get($data, $key, $default);
        case $data instanceof Collection:
            return $data->get($key, $default);
        case $data instanceof Arrayable:
            return Arr::get($data->toArray(), $key, $default);
        case $data instanceof \ArrayIterator:
            return $data->getArrayCopy()[$key] ?? $default;
        case $data instanceof \ArrayAccess:
            return $data[$key] ?? $default;
        case $data instanceof \IteratorAggregate && $data->getIterator() instanceof \ArrayIterator:
            return $data->getIterator()->getArrayCopy()[$key] ?? $default;
        case is_object($data):
            return $data->{$key} ?? $default;
        default:
            throw new RuntimeException(sprintf('Can\'t access data with key "%s"', $key));
    }
}

/**
 * @param $data
 * @return array
 * @throws RuntimeException
 */
function data_to_array($data)
{
    switch (true) {
        case is_array($data):
            return $data;
        case $data instanceof Collection:
            return $data->all();
        case $data instanceof Arrayable:
            return $data->toArray();
        case $data instanceof \IteratorAggregate && $data->getIterator() instanceof \ArrayIterator:
            return $data->getIterator()->getArrayCopy();
        case $data instanceof \ArrayIterator:
            return $data->getArrayCopy();
        default:
            throw new RuntimeException(sprintf('Can\'t transform data to array'));
    }
}

