<?php

namespace EasyAlipay\Kernel;

use EasyAlipay\Kernel\Exceptions\BadRequestException;
use EasyAlipay\Kernel\Exceptions\InvalidArgumentException;
use EasyAlipay\Kernel\Exceptions\InvalidConfigException;
use EasyAlipay\Kernel\Exceptions\InvalidSignException;
use EasyAlipay\Kernel\Support\Collection;
use EasyAlipay\Kernel\Traits\SingData;
use function EasyAlipay\Kernel\encrypt;
use function EasyAlipay\Kernel\decrypt;

class AopClient extends BaseClient
{
    use SingData;

    /**
     * 应用ID
     * @var string
     */
    private $appId;

    /**
     * 商户私钥值
     * @var string
     */
    private $rsaPrivateKey;

    /**
     * 支付宝公钥值
     * @var string
     */
    private $alipayrsaPublicKey;

    /**
     * 支付宝网关
     * @var string
     */
    private $gatewayUrl = "https://openapi.alipay.com/gateway.do";
    //返回数据格式
    /**
     * 返回数据格式
     * @var string
     */
    private $format = "json";

    /**
     * 签名类型
     * @var string
     */
    private $signType = "RSA2";

    /**
     * 接口版本
     * @var string
     */
    private $apiVersion = "1.0";

    /**
     * 是否调试
     * @var bool
     */
    private $debugInfo = false;

    /**
     * 返回后缀
     * @var string
     */
    private $RESPONSE_SUFFIX = "_response";

    /**
     * 错误后缀
     * @var string
     */
    private $ERROR_RESPONSE = "error_response";

    /**
     * 签名字段名称
     * @var string
     */
    private $SIGN_NODE_NAME = "sign";

    /**
     * 加密密钥
     * @var string
     */
    private $encryptKey;

    /**
     * 加密类型
     * @var string
     */
    private $encryptType = "AES";
    /**
     * sdk版本
     * @var string
     */
    protected $alipaySdkVersion = "alipay-sdk-php-easyalipay-20190820";

    /**
     * 配置信息
     * @var object
     */
    protected $config;

    /**
     * AopClient constructor.
     * @param ServiceContainer $app
     * @throws InvalidConfigException
     */
    public function __construct(ServiceContainer $app)
    {
        parent::__construct($app);
        $this->config = $app['config'];

        $this->appId = $app['config']['app_id'];
        $this->rsaPrivateKey = $app['config']['merchant_private_key'];
        $this->alipayrsaPublicKey = $app['config']['alipay_public_key'];
        $this->postCharset = $app['config']['charset'];
        $this->signType = $app['config']['sign_type'];
        $this->encryptKey = $app['config']['encrypt_key'];
        if (empty($this->appId) || trim($this->appId) == "") {
            throw new InvalidConfigException("appId should not be NULL!");
        }
        if (empty($this->rsaPrivateKey) || trim($this->rsaPrivateKey) == "") {
            throw new InvalidConfigException("rsaPrivateKey should not be NULL!");
        }
        if (empty($this->alipayrsaPublicKey) || trim($this->alipayrsaPublicKey) == "") {
            throw new InvalidConfigException("alipayPublicKey should not be NULL!");
        }
        if (empty($this->signType) || trim($this->signType) == "") {
            throw new InvalidConfigException("signType should not be NULL!");
        }
    }

    /**
     * @param AopRequest $request
     * @param null $authToken
     * @param null $appInfoAuthtoken
     * @return Collection
     * @throws BadRequestException
     * @throws InvalidArgumentException
     * @throws InvalidConfigException
     * @throws InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function execute(AopRequest $request, $authToken = null)
    {
        $this->setupCharsets($request);
        if (strcasecmp($this->fileCharset, $this->postCharset)) {
            throw new InvalidConfigException("文件编码：[" . $this->fileCharset . "] 与表单提交编码：[" . $this->postCharset . "]两者不一致!");
        }
        $iv = null;
        if (!$this->checkEmpty($request->getApiVersion())) {
            $iv = $request->getApiVersion();
        } else {
            $iv = $this->apiVersion;
        }
        $sysParams["app_id"] = $this->appId;
        $sysParams["version"] = $iv;
        $sysParams["format"] = $this->format;
        $sysParams["sign_type"] = $this->signType;
        $sysParams["method"] = $request->getApiMethodName();
        $sysParams["timestamp"] = date("Y-m-d H:i:s");
        $sysParams["alipay_sdk"] = $this->alipaySdkVersion;
        $sysParams["terminal_type"] = $request->getTerminalType();
        $sysParams["terminal_info"] = $request->getTerminalInfo();
        $sysParams["prod_code"] = $request->getProdCode();
        $sysParams["notify_url"] = $request->getNotifyUrl();
        $sysParams["charset"] = $this->postCharset;
        $sysParams["app_auth_token"] = $authToken;
        //获取业务参数
        $apiParams = $request->getApiParas();
        if (method_exists($request, "getNeedEncrypt") && $request->getNeedEncrypt()) {
            $sysParams["encrypt_type"] = $this->encryptType;
            $apiParams['biz_content'] = $this->encryptCode($apiParams['biz_content']);
        }
        //签名
        $sysParams["sign"] = $this->generateSign(array_merge($apiParams, $sysParams), $this->signType);
        $requestUrl = $this->buildRequestUrl($sysParams);
        $result = $this->httpPost($requestUrl, $apiParams);
        $apiName = $request->getApiMethodName();
        $method = str_replace(".", "_", $apiName) . $this->RESPONSE_SUFFIX;
        $content = $result[$method];
        if (method_exists($request, "getNeedEncrypt") && $request->getNeedEncrypt()) {
            $enCryptContent = $this->encryptCode($apiParams['biz_content'], 'decrypt');
            $content = json_decode($enCryptContent, 1);
        }
        if (!isset($result[$this->SIGN_NODE_NAME]) || $content['code'] != '10000') {
            throw new BadRequestException(
                'Get Alipay API Error:' . $content['msg'] .
                (isset($content['sub_code']) ? (' - ' . $content['sub_code']) : '')
            );
        }
        if (!$this->verify($content, $result[$this->SIGN_NODE_NAME], $this->alipayrsaPublicKey, $this->signType)) {
            throw new InvalidSignException('签名失败');
        }
        return new Collection($content);
    }

    /**
     * 加密｜解密
     * @param $content
     * @param string $operation
     * @return false|string
     * @throws InvalidArgumentException
     */
    public function encryptCode($content, string $operation = 'encrypt')
    {
        if ($this->checkEmpty($content)) {
            throw new InvalidArgumentException("content Fail!");
        }
        if ($this->checkEmpty($this->encryptKey) || $this->checkEmpty($this->encryptType)) {
            throw new InvalidArgumentException(" encryptType and encryptKey must not null! ");
        }
        if ("AES" != $this->encryptType) {
            throw new InvalidArgumentException("加密类型只支持AES");
        }
        if ($operation == 'encrypt') {
            $enCryptContent = encrypt($content, $this->encryptKey);
        } else {
            $enCryptContent = decrypt($content, $this->encryptKey);
        }
        return $enCryptContent;
    }

    /**
     * 构造请求url
     * @param $sysParams
     * @return false|string
     */
    public function buildRequestUrl($sysParams)
    {
        $requestUrl = $this->gatewayUrl . "?";
        foreach ($sysParams as $sysParamKey => $sysParamValue) {
            $requestUrl .= "$sysParamKey=" . urlencode($this->characet($sysParamValue, $this->postCharset)) . "&";
        }
        $requestUrl = substr($requestUrl, 0, -1);
        return $requestUrl;
    }

    /**
     * 获取签名数据
     * @param $params
     * @param string $signType
     * @return string
     * @throws InvalidConfigException
     */
    public function generateSign($params, $signType = "RSA")
    {
        return $this->sign($this->getSignContent($params), $this->rsaPrivateKey, $signType);
    }

    /**
     * 设置编码
     * @param AopRequest $request
     */
    private function setupCharsets(AopRequest $request)
    {
        if ($this->checkEmpty($this->postCharset)) {
            $this->postCharset = 'UTF-8';
        }
        $str = preg_match('/[\x80-\xff]/', $this->appId) ? $this->appId : print_r($request, true);
        $this->fileCharset = mb_detect_encoding($str, "UTF-8, GBK") == 'UTF-8' ? 'UTF-8' : 'GBK';
    }
}
