<?php

namespace EasyAlipay\Kernel\Events;

use EasyAlipay\Kernel\AccessToken;

/**
 * Class AccessTokenRefreshed.
 *
 * @author mingyoung <mingyoungcheung@gmail.com>
 */
class AccessTokenRefreshed
{
    /**
     * @var \EasyAlipay\Kernel\AccessToken
     */
    public $accessToken;

    /**
     * @param \EasyAlipay\Kernel\AccessToken $accessToken
     */
    public function __construct(AccessToken $accessToken)
    {
        $this->accessToken = $accessToken;
    }
}
