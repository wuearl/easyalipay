<?php
/**
 * Created by PhpStorm.
 * User: wuliangbo
 * Date: 2020/1/6
 * Time: 14:25
 */

namespace EasyAlipay\Kernel\Exceptions;

use Exception as BaseException;

class Exception extends BaseException
{
}
