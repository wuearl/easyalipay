<?php
/**
 * Created by PhpStorm.
 * User: wuliangbo
 * Date: 2020/1/7
 * Time: 10:15
 */

namespace EasyAlipay\Kernel\Traits;

use EasyAlipay\Kernel\FileForm\FileField;
use EasyAlipay\Kernel\FileForm\FileForm;
use EasyAlipay\Kernel\Exceptions\InvalidConfigException;
use EasyAlipay\Kernel\Support\Str;
use GuzzleHttp\Psr7\Stream;

trait SingData
{
    public $postCharset = 'UTF-8';
    public $fileCharset = 'UTF-8';

    /**
     * 签名
     * @param $data
     * @param $privateKey
     * @param string $signType
     * @return string
     * @throws InvalidConfigException
     */
    protected function sign($data, $privateKey, $signType = "RSA")
    {
        if (is_null($privateKey)) {
            throw new InvalidConfigException('Missing Alipay Config -- [private_key]');
        }

        if (Str::endsWith($privateKey, '.pem')) {
            $privateKey = openssl_pkey_get_private(
                Str::startsWith($privateKey, 'file://') ? $privateKey : 'file://'.$privateKey
            );
        } else {
            $privateKey = "-----BEGIN RSA PRIVATE KEY-----\n".
                wordwrap($privateKey, 64, "\n", true).
                "\n-----END RSA PRIVATE KEY-----";
        }

        if ("RSA2" == $signType) {
            openssl_sign($data, $sign, $privateKey, OPENSSL_ALGO_SHA256);
        } else {
            openssl_sign($data, $sign, $privateKey);
        }
        if (is_resource($privateKey)) {
            openssl_free_key($privateKey);
        }
        $sign = base64_encode($sign);
        return $sign;
    }

    /**
     * @param $data
     * @param $sign
     * @param $publicKey
     * @param string $signType
     * @return bool
     * @throws InvalidConfigException
     */
    public function verify($data, $sign, $publicKey, $signType = 'RSA')
    {
        if (is_null($publicKey)) {
            throw new InvalidConfigException('Missing Alipay Config -- [ali_public_key]');
        }
        if (Str::endsWith($publicKey, '.crt')) {
            $publicKey = file_get_contents($publicKey);
        } elseif (Str::endsWith($publicKey, '.pem')) {
            $publicKey = openssl_pkey_get_public(
                Str::startsWith($publicKey, 'file://') ? $publicKey : 'file://'.$publicKey
            );
        } else {
            $publicKey = "-----BEGIN PUBLIC KEY-----\n".
                wordwrap($publicKey, 64, "\n", true).
                "\n-----END PUBLIC KEY-----";
        }
        $data = json_encode($data, JSON_UNESCAPED_UNICODE);
        if ("RSA2" == $signType) {
            $result = (openssl_verify($data, base64_decode($sign), $publicKey, OPENSSL_ALGO_SHA256) === 1);
        } else {
            $result = (openssl_verify($data, base64_decode($sign), $publicKey) === 1);
        }
        if (is_resource($publicKey)) {
            //释放资源
            openssl_free_key($publicKey);
        }
        return $result;
    }

    public function rsaCheckV2($data, $sign, $publicKey,$signType='RSA') {
        ksort($data);
        $data = $this->getSignContent($data);
        if (is_null($publicKey)) {
            throw new InvalidConfigException('Missing Alipay Config -- [ali_public_key]');
        }
        if (Str::endsWith($publicKey, '.crt')) {
            $publicKey = file_get_contents($publicKey);
        } elseif (Str::endsWith($publicKey, '.pem')) {
            $publicKey = openssl_pkey_get_public(
                Str::startsWith($publicKey, 'file://') ? $publicKey : 'file://'.$publicKey
            );
        } else {
            $publicKey = "-----BEGIN PUBLIC KEY-----\n".
                wordwrap($publicKey, 64, "\n", true).
                "\n-----END PUBLIC KEY-----";
        }
        if ("RSA2" == $signType) {
            $result = (openssl_verify($data, base64_decode($sign), $publicKey, OPENSSL_ALGO_SHA256) === 1);
        } else {
            $result = (openssl_verify($data, base64_decode($sign), $publicKey) === 1);
        }
        if (is_resource($publicKey)) {
            //释放资源
            openssl_free_key($publicKey);
        }
        return $result;
    }
    /**
     * 获取签名文本
     * @param $params
     * @return string
     */
    public function getSignContent($params)
    {
        ksort($params);
        $stringToBeSigned = "";
        $i = 0;
        foreach ($params as $k => $v) {
            if (false === $this->checkEmpty($v) && "@" != substr($v, 0, 1)) {
                // 转换成目标字符集
                $v = $this->characet($v, $this->postCharset);

                if ($i == 0) {
                    $stringToBeSigned .= "$k" . "=" . "$v";
                } else {
                    $stringToBeSigned .= "&" . "$k" . "=" . "$v";
                }
                $i++;
            }
        }
        unset ($k, $v);
        return $stringToBeSigned;
    }

    /**
     * 转换字符集编码
     * @param $data
     * @param $targetCharset
     * @return string
     */
    function characet($data, $targetCharset)
    {
        if (!empty($data)) {
            $fileType = $this->fileCharset;
            if (strcasecmp($fileType, $targetCharset) != 0) {
                $data = mb_convert_encoding($data, $targetCharset, $fileType);
            }
        }
        return $data;
    }

    /**
     * 校验$value是否非空
     *  if not set ,return true;
     *    if is null , return true;
     **/
    protected function checkEmpty($value)
    {
        if (!isset($value))
            return true;
        if ($value === null)
            return true;
        if (trim($value) === "")
            return true;
        return false;
    }

    /**
     * 生成应用证书SN.
     * @param $cert_path
     * @return string
     * @throws \Exception
     */
    public function getCertSN($cert_path): string
    {
        if (!is_file($cert_path)) {
            throw new \Exception('unknown certPath -- [getCertSN]');
        }
        $x509data = file_get_contents($cert_path);
        if (false === $x509data) {
            throw new \Exception('Alipay CertSN Error -- [getCertSN]');
        }
        openssl_x509_read($x509data);
        $cert_data = openssl_x509_parse($x509data);
        if (empty($cert_data)) {
            throw new \Exception('Alipay openssl_x509_parse Error -- [getCertSN]');
        }
        $issuer_arr = [];
        foreach ($cert_data['issuer'] as $key => $val) {
            $issuer_arr[] = $key.'='.$val;
        }
        $issuer = implode(',', array_reverse($issuer_arr));

        return md5($issuer. $cert_data['serialNumber']);
    }

    /**
     * 生成支付宝根证书SN.
     *
     * @param $certPath
     * @return string
     * @throws /Exception
     */
    public function getRootCertSN($certPath)
    {
        if (!is_file($certPath)) {
            throw new \Exception('unknown certPath -- [getRootCertSN]');
        }
        $x509data = file_get_contents($certPath);
        if (false === $x509data) {
            throw new \Exception('Alipay CertSN Error -- [getRootCertSN]');
        }
        $kCertificateEnd = '-----END CERTIFICATE-----';
        $certStrList = explode($kCertificateEnd, $x509data);
        $md5_arr = [];
        foreach ($certStrList as $one) {
            if (!empty(trim($one))) {
                $_x509data = $one.$kCertificateEnd;
                openssl_x509_read($_x509data);
                $_certdata = openssl_x509_parse($_x509data);
                if (in_array($_certdata['signatureTypeSN'], ['RSA-SHA256', 'RSA-SHA1'])) {
                    $issuer_arr = [];
                    foreach ($_certdata['issuer'] as $key => $val) {
                        $issuer_arr[] = $key.'='.$val;
                    }
                    $_issuer = implode(',', array_reverse($issuer_arr));
                    if (0 === strpos($_certdata['serialNumber'], '0x')) {
                        $serialNumber = self::bchexdec($_certdata['serialNumber']);
                    } else {
                        $serialNumber = $_certdata['serialNumber'];
                    }
                    $md5_arr[] = md5($_issuer.$serialNumber);
                }
            }
        }

        return implode('_', $md5_arr);
    }
    /**
     * 0x转高精度数字.
     *
     * @param $hex
     * @return int|string
     */
    private static function bchexdec($hex)
    {
        $dec = 0;
        $len = strlen($hex);
        for ($i = 1; $i <= $len; ++$i) {
            if (ctype_xdigit($hex[$i - 1])) {
                $dec = bcadd($dec, bcmul(strval(hexdec($hex[$i - 1])), bcpow('16', strval($len - $i))));
            }
        }

        return $dec;
    }
    /**
     * 文件上传
     *
     * @param array $textParams
     * @param array $files
     * @param $boundary
     * @return \EasyAlipay\Kernel\FileForm\FileFormStream
     */
    public function _toMultipartRequestBody(array $textParams, array $files, $boundary)
    {
        $map = $textParams;
        foreach ($files as $k => $v) {
            $fileField = new FileField();
            $fileField->filename = $v;
            $fileField->contentType = 'multipart/form-data;charset=utf-8;boundary=' . $boundary;
            $fileField->content = new Stream(fopen($v, 'r'));
            $map[$k] = $fileField;
        }
        $stream = FileForm::toFileForm($map, $boundary);
        do {
            $readLength = $stream->read(1024);
        } while (0 != $readLength);
        return $stream;
    }

    /**
     * @return string
     */
    protected function _getRandomBoundary()
    {
        return date("Y-m-d H:i:s") . '';
    }
}