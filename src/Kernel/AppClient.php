<?php

namespace EasyAlipay\Kernel;

use EasyAlipay\Kernel\Exceptions\BadRequestException;
use EasyAlipay\Kernel\Exceptions\InvalidArgumentException;
use EasyAlipay\Kernel\Exceptions\InvalidConfigException;
use EasyAlipay\Kernel\Exceptions\InvalidSignException;
use EasyAlipay\Kernel\Support\Collection;
use EasyAlipay\Kernel\Traits\SingData;
use function EasyAlipay\Kernel\decrypt;
use function EasyAlipay\Kernel\encrypt;

class AppClient extends BaseClient
{
    use SingData;

    /**
     * 应用ID
     * @var string
     */
    protected $appId;

    /**
     * 商户私钥值
     * @var string
     */
    protected $rsaPrivateKey;

    /**
     * 支付宝公钥值
     * @var string
     */
    protected $alipayrsaPublicKey;

    /**
     * 支付宝网关
     * @var string
     */
    private $gatewayUrl = "https://openapi.alipay.com/gateway.do";
    //返回数据格式
    /**
     * 返回数据格式
     * @var string
     */
    private $format = "json";

    /**
     * 签名类型
     * @var string
     */
    protected $signType = "RSA2";

    /**
     * 接口版本
     * @var string
     */
    private $apiVersion = "1.0";

    /**
     * 是否调试
     * @var bool
     */
    private $debugInfo = false;

    /**
     * 返回后缀
     * @var string
     */
    private $RESPONSE_SUFFIX = "_response";

    /**
     * 错误后缀
     * @var string
     */
    private $ERROR_RESPONSE = "error_response";

    /**
     * 签名字段名称
     * @var string
     */
    private $SIGN_NODE_NAME = "sign";

    /**
     * 加密密钥
     * @var string
     */
    private $encryptKey;

    /**
     * 加密类型
     * @var string
     */
    private $encryptType = "AES";
    /**
     * sdk版本
     * @var string
     */
    protected $alipaySdkVersion = "alipay-sdk-php-easyalipay-20190820";

    /**
     * 配置信息
     * @var object
     */
    protected $config;

    /**
     * AopClient constructor.
     * @param ServiceContainer $app
     * @throws InvalidConfigException
     */
    public function __construct(ServiceContainer $app)
    {
        parent::__construct($app);
        $this->config = $app['config'];

        $this->appId = $app['config']['app_id'];
        $this->rsaPrivateKey = $app['config']['merchant_private_key'];
        $this->alipayrsaPublicKey = $app['config']['alipay_public_key'];
        $this->postCharset = $app['config']['charset'];
        $this->signType = $app['config']['sign_type'];
        $this->encryptKey = $app['config']['encrypt_key'];
        if (empty($this->appId) || trim($this->appId) == "") {
            throw new InvalidConfigException("appId should not be NULL!");
        }
        if (empty($this->rsaPrivateKey) || trim($this->rsaPrivateKey) == "") {
            throw new InvalidConfigException("rsaPrivateKey should not be NULL!");
        }
        if (empty($this->alipayrsaPublicKey) || trim($this->alipayrsaPublicKey) == "") {
            throw new InvalidConfigException("alipayPublicKey should not be NULL!");
        }
        if (empty($this->signType) || trim($this->signType) == "") {
            throw new InvalidConfigException("signType should not be NULL!");
        }
    }

    /**
     * @param AppRequest $request
     * @return Collection
     * @throws BadRequestException
     * @throws InvalidArgumentException
     * @throws InvalidConfigException
     * @throws InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Exception
     */
    public function execute(AppRequest $request)
    {
        $this->setupCharsets($request);
        if (strcasecmp($this->fileCharset, $this->postCharset)) {
            throw new InvalidConfigException("文件编码：[" . $this->fileCharset . "] 与表单提交编码：[" . $this->postCharset . "]两者不一致!");
        }
        $sysParams = $this->buildParams($request);

        //获取业务参数
        $apiParams = $request->getApiParas();
        if (method_exists($request, "getNeedEncrypt") && $request->getNeedEncrypt()) {
            $sysParams["encrypt_type"] = $this->encryptType;
            $apiParams['biz_content'] = $this->encryptCode($apiParams['biz_content']);
        }
        $postMultipart = false;
        $files = [];
        foreach ($apiParams as $k => $v) {
            if ("@" == substr($v, 0, 1)) {
                $files[$k] = substr($v, 1);
                unset($apiParams[$k]);
                $postMultipart = true;
            }
        }
        //签名
        $sysParams["sign"] = $this->generateSign(array_merge($apiParams, $sysParams), $this->signType);
        $requestUrl = $this->buildRequestUrl($sysParams);
        if ($postMultipart) {
            $result = $this->httpUpload($requestUrl, $apiParams, $files);
        } else {
            $result = $this->httpPost($requestUrl, $apiParams);
        }
        if (isset($result[$this->ERROR_RESPONSE])) { //返回错误
            throw new BadRequestException(
                'Get Alipay API Error:' . $result[$this->ERROR_RESPONSE]['msg'] .
                (isset($result[$this->ERROR_RESPONSE]['sub_code']) ? (' - ' . $result[$this->ERROR_RESPONSE]['sub_code']) : '')
            );
        }
        $apiName = $request->getApiMethodName();
        $method = str_replace(".", "_", $apiName) . $this->RESPONSE_SUFFIX;
        $content = $result[$method];
        if (method_exists($request, "getNeedEncrypt") && $request->getNeedEncrypt()) {
            $enCryptContent = $this->encryptCode($apiParams['biz_content'], 'decrypt');
            $content = json_decode($enCryptContent, 1);
        }
        if (!isset($result[$this->SIGN_NODE_NAME]) || (isset($content['code']) && $content['code'] != '10000')) {
            throw new BadRequestException(
                'Get Alipay API Error:' . $content['msg'] .
                (isset($content['sub_code']) ? (' - ' . $content['sub_code']) : '')
            );
        }
        if (!$this->verify($content, $result[$this->SIGN_NODE_NAME], $this->alipayrsaPublicKey, $this->signType)) {
            throw new InvalidSignException('签名失败');
        }
        return new Collection($content);
    }

    /**
     * @param AppRequest $request
     * @return string
     * @throws InvalidConfigException
     * @throws \Exception
     */
    public function sdkExecute(AppRequest $request)
    {
        $this->setupCharsets($request);
        $sysParams = $this->buildParams($request);
        $apiParams = $request->getApiParas();
        $sysParams = array_merge($sysParams, $apiParams);
        ksort($sysParams);
        $sysParams["sign"] = $this->generateSign($sysParams, $this->signType);
        return http_build_query($sysParams);
    }

    /**
     * @param AppRequest $request
     * @return array
     * @throws \Exception
     */
    protected function buildParams(AppRequest $request): array
    {
        if (!$this->checkEmpty($request->getApiVersion())) {
            $iv = $request->getApiVersion();
        } else {
            $iv = $this->apiVersion;
        }
        $sysParams["app_id"] = $this->appId;
        $sysParams["version"] = $iv;
        $sysParams["format"] = $this->format;
        $sysParams["sign_type"] = $this->signType;
        $sysParams["method"] = $request->getApiMethodName();
        $sysParams["timestamp"] = date("Y-m-d H:i:s");
        $sysParams["alipay_sdk"] = $this->alipaySdkVersion;
        if ($request->getTerminalType()) {
            $sysParams["terminal_type"] = $request->getTerminalType();
        }
        if ($request->getTerminalInfo()) {
            $sysParams["terminal_info"] = $request->getTerminalInfo();
        }
        if ($request->getProdCode()) {
            $sysParams["prod_code"] = $request->getProdCode();
        }
        $sysParams["notify_url"] = $request->getNotifyUrl();
        $sysParams["charset"] = $this->postCharset;
        if (isset($this->config['auth_token'])) {
            $sysParams["auth_token"] = $this->config['auth_token'];
        }
        if ($this->app->config->get('app_cert_public_key') && $this->app->config->get('alipay_root_cert')) {
            $sysParams['app_cert_sn'] = $this->getCertSN($this->app->config->get('app_cert_public_key'));
            $sysParams['alipay_root_cert_sn'] = $this->getRootCertSN($this->app->config->get('alipay_root_cert'));
        }
        if (isset($this->config['app_auth_token'])) {
            $sysParams["app_auth_token"] = $this->config['app_auth_token'];
        }

        return $sysParams;
    }

    /**
     * 加密｜解密
     * @param $content
     * @param string $operation
     * @return false|string
     * @throws InvalidArgumentException
     */
    public function encryptCode($content, string $operation = 'encrypt')
    {
        if ($this->checkEmpty($content)) {
            throw new InvalidArgumentException("content Fail!");
        }
        if ($this->checkEmpty($this->encryptKey) || $this->checkEmpty($this->encryptType)) {
            throw new InvalidArgumentException(" encryptType and encryptKey must not null! ");
        }
        if ("AES" != $this->encryptType) {
            throw new InvalidArgumentException("加密类型只支持AES");
        }
        if ($operation == 'encrypt') {
            $enCryptContent = encrypt($content, $this->encryptKey);
        } else {
            $enCryptContent = decrypt($content, $this->encryptKey);
        }
        return $enCryptContent;
    }

    /**
     * 构造请求url
     * @param $sysParams
     * @return false|string
     */
    public function buildRequestUrl($sysParams)
    {
        return "?" . http_build_query($sysParams);
    }

    /**
     * 获取签名数据
     * @param $params
     * @param string $signType
     * @return string
     * @throws InvalidConfigException
     */
    public function generateSign($params, $signType = "RSA")
    {
        return $this->sign($this->getSignContent($params), $this->rsaPrivateKey, $signType);
    }

    /**
     * 设置编码
     * @param AppRequest $request
     */
    private function setupCharsets(AppRequest $request)
    {
        if ($this->checkEmpty($this->postCharset)) {
            $this->postCharset = 'UTF-8';
        }
        $str = preg_match('/[\x80-\xff]/', $this->appId) ? $this->appId : print_r($request, true);
        $this->fileCharset = mb_detect_encoding($str, "UTF-8, GBK") == 'UTF-8' ? 'UTF-8' : 'GBK';
    }
}
