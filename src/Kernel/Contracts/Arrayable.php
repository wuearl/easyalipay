<?php
/**
 * Created by PhpStorm.
 * User: wuliangbo
 * Date: 2020/1/6
 * Time: 14:53
 */

namespace EasyAlipay\Kernel\Contracts;

use ArrayAccess;

interface Arrayable extends ArrayAccess
{
    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray();
}
