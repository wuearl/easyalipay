<?php

namespace EasyAlipay\Kernel;

class AppRequest
{
    /**
     * 接口名称
     * @var string
     */
    protected $apiMethodName;

    /**
     * 请求参数集合
     * @var string
     */

    protected $bizContent;

    /**
     * 请求参数数组
     * @var array
     */
    protected $apiParas = array();

    /**
     * 终端类型
     * @var string
     */
    protected $terminalType;

    /**
     * 终端信息
     * @var string
     */
    protected $terminalInfo;

    /**
     * 产品代码
     * @var string
     */
    protected $prodCode;

    /**
     * 接口版本
     * @var string
     */
    protected $apiVersion = "1.0";

    /**
     * 异步通知地址
     * @var string
     */
    protected $notifyUrl;

    /**
     * 回调地址
     * @var string
     */
    protected $returnUrl;

    /**
     * 是否需要加密
     * @var bool
     */
    protected $needEncrypt = false;

    /**
     * 获取接口名称
     * @return string
     */
    public function getApiMethodName()
    {
        return $this->apiMethodName;
    }

    /**
     * 设置接口名称
     * @param $apiMethodName
     */
    public function setApiMethodName($apiMethodName)
    {
        $this->apiMethodName = $apiMethodName;
    }

    /**
     * 设置请求参数集合
     * @param $bizContent
     */
    public function setBizContent($bizContent)
    {
        $this->bizContent = $bizContent;
        $this->apiParas["biz_content"] = $bizContent;
    }

    /**
     * 设置请求参数集合
     * @return string
     */
    public function getBizContent()
    {
        return $this->bizContent;
    }

    /**
     * 设置异步通知地址
     * @param $notifyUrl
     */
    public function setNotifyUrl($notifyUrl)
    {
        $this->notifyUrl = $notifyUrl;
    }

    /**
     * 获取异步通知地址
     * @return string
     */
    public function getNotifyUrl()
    {
        return $this->notifyUrl;
    }

    /**
     * 设置回调地址
     * @param $returnUrl
     */
    public function setReturnUrl($returnUrl)
    {
        $this->returnUrl = $returnUrl;
    }

    /**
     * 获取回调地址
     * @return string
     */
    public function getReturnUrl()
    {
        return $this->returnUrl;
    }

    /**
     * 获取请求参数数组
     * @return array
     */
    public function getApiParas()
    {
        return $this->apiParas;
    }

    /**
     * 获取终端类型
     * @return string
     */
    public function getTerminalType()
    {
        return $this->terminalType;
    }

    /**
     * 设置终端类型
     * @param $terminalType
     */
    public function setTerminalType($terminalType)
    {
        $this->terminalType = $terminalType;
    }

    /**
     * 获取终端信息
     * @return string
     */
    public function getTerminalInfo()
    {
        return $this->terminalInfo;
    }

    /**
     * 设置终端信息
     * @param $terminalInfo
     */
    public function setTerminalInfo($terminalInfo)
    {
        $this->terminalInfo = $terminalInfo;
    }

    /**
     * 获取产品代码
     * @return string
     */
    public function getProdCode()
    {
        return $this->prodCode;
    }

    /**
     * 设置产品代码
     * @param $prodCode
     */
    public function setProdCode($prodCode)
    {
        $this->prodCode = $prodCode;
    }

    /**
     * 设置接口版本
     * @param $apiVersion
     */
    public function setApiVersion($apiVersion)
    {
        $this->apiVersion = $apiVersion;
    }

    /**
     * 获取接口版本
     * @return string
     */
    public function getApiVersion()
    {
        return $this->apiVersion;
    }

    /**
     * 设置加密
     * @param $needEncrypt
     */
    public function setNeedEncrypt($needEncrypt)
    {
        $this->needEncrypt = $needEncrypt;
    }

    /**
     * 获取加密
     * @return bool
     */
    public function getNeedEncrypt()
    {
        return $this->needEncrypt;
    }
}
