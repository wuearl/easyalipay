<?php

namespace EasyAlipay\Marketing\Pass;

use EasyAlipay\Kernel\Support;
use EasyAlipay\Kernel\AppClient;
use EasyAlipay\Kernel\AppRequest;
use EasyAlipay\Kernel\Config;
use EasyAlipay\Marketing\Model\AlipayPassTemplateAddContentBuilder;
use EasyAlipay\Marketing\Model\AlipayPassTemplateUpdateContentBuilder;
use EasyAlipay\Marketing\Model\AlipayPassInstanceAddContentBuilder;
use EasyAlipay\Marketing\Model\AlipayPassInstanceUpdateContentBuilder;

class Client extends AppClient
{
    /**
     * @param string $unique_id
     * @param string $tpl_content
     * @return Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function createTemplate(string $unique_id,string $tpl_content)
    {
        //构造查询业务请求参数对象
        $templateAddContentBuilder = new AlipayPassTemplateAddContentBuilder();
        $templateAddContentBuilder->setUniqueId($unique_id);
        $templateAddContentBuilder->setTplContent($tpl_content);
        $request = new AppRequest ();
        $request->setBizContent($templateAddContentBuilder->getBizContent());
        $request->setApiMethodName("alipay.pass.template.add");
        return($this->execute($request)) ;
    }

    /**
     * @param string $tpl_id
     * @param string $tpl_content
     * @return Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updateTemplate(string $tpl_id,string $tpl_content)
    {
        //构造查询业务请求参数对象
        $templateUpdateContentBuilder = new AlipayPassTemplateUpdateContentBuilder();
        $templateUpdateContentBuilder->setTplId($tpl_id);
        $templateUpdateContentBuilder->setTplContent($tpl_content);
        $request = new AppRequest ();
        $request->setBizContent($templateUpdateContentBuilder->getBizContent());
        $request->setApiMethodName("alipay.pass.template.update");
        return($this->execute($request)) ;
    }

    /**
     * @param string $tpl_id
     * @param string $tpl_params
     * @param string $recognition_type
     * @param string $recognition_info
     * @return Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function addInstance(string $tpl_id,string $tpl_params,string $recognition_type,string $recognition_info)
    {
        //构造查询业务请求参数对象
        $addInstanceContentBuilder = new AlipayPassInstanceAddContentBuilder();
        $addInstanceContentBuilder->setTplId($tpl_id);
        $addInstanceContentBuilder->setTplParams($tpl_params);
        $addInstanceContentBuilder->setRecognitionType($recognition_type);
        $addInstanceContentBuilder->setRecognitionInfo($recognition_info);
        $request = new AppRequest ();
        $request->setBizContent($addInstanceContentBuilder->getBizContent());
        $request->setApiMethodName("alipay.pass.instance.add");
        return($this->execute($request)) ;
    }

    /**
     * @param string $serial_number
     * @param string $channel_id
     * @param string $tpl_params
     * @param string $status
     * @param string $verify_code
     * @param string $verify_type
     * @return Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function updateInstance(string $serial_number,string $channel_id,string $tpl_params,string $status,string $verify_code,string $verify_type)
    {
        //构造查询业务请求参数对象
        $updateInstanceContentBuilder = new AlipayPassInstanceUpdateContentBuilder();
        $updateInstanceContentBuilder->setSerialNumber($serial_number);
        $updateInstanceContentBuilder->setChannelId($channel_id);
        $updateInstanceContentBuilder->setTplParams($tpl_params);
        $updateInstanceContentBuilder->setStatus($status);
        $updateInstanceContentBuilder->setVerifyCode($verify_code);
        $updateInstanceContentBuilder->setVerifyType($verify_type);
        $request = new AppRequest ();
        $request->setBizContent($updateInstanceContentBuilder->getBizContent());
        $request->setApiMethodName("alipay.pass.instance.update");
        return($this->execute($request)) ;
    }

}
