<?php
/**
 * Created by PhpStorm.
 * User: wuliangbo
 * Date: 2020/1/9
 * Time: 11:46
 */

namespace EasyAlipay\OpenPlatform\Model;

use EasyAlipay\Payment\Model\BaseContentBuilder;

class AlipayOpenAgentCreateContentBuilder extends BaseContentBuilder
{
    /**
     * isv代操作的商户账号: 支付宝账号|pid
     * @var string
     */
    protected $account;

    /**
     * 商户联系人信息
     * @var array
     */
    protected $contactInfo;

    /**
     * 订单授权凭证
     * @var string
     */
    protected $orderTicket;

    /**
     * 获取商户账号
     * @return string
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * 设置商户账号
     * @param $account
     */
    public function setAccount($account)
    {
        $this->account = $account;
        $this->bizContentarr['account'] = $account;
    }

    /**
     * 获取商户联系人信息
     * @return array
     */
    public function getContactInfo()
    {
        return $this->contactInfo;
    }

    /**
     * 设置商户联系人信息
     * @param $contactInfo
     */
    public function setContactInfo($contactInfo)
    {
        $this->contactInfo = $contactInfo;
        $this->bizContentarr['contact_info'] = $contactInfo;
    }

    /**
     * 获取订单授权凭证
     * @return string
     */
    public function getOrderTicket()
    {
        return $this->orderTicket;
    }

    /**
     * 设置订单授权凭证
     * @param $orderTicket
     */
    public function setOrderTicket($orderTicket)
    {
        $this->orderTicket = $orderTicket;
        $this->bizContentarr['order_ticket'] = $orderTicket;
    }
}