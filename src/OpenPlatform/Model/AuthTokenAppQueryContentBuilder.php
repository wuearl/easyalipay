<?php
/**
 * Created by PhpStorm.
 * User: wuliangbo
 * Date: 2020/3/13
 * Time: 18:03
 */

namespace EasyAlipay\OpenPlatform\Model;
class AuthTokenAppQueryContentBuilder
{
    /**
     * @var string 应用授权令牌
     */
    protected $appAuthToken;

    /**
     * 请求参数数组
     * @var array
     */
    protected $bizContentarr = array();

    /**
     * 请求参数字符串
     * @var null|string
     */
    protected $bizContent = NULL;

    /**
     * 获取参数集合
     * @return false|string|null
     */
    public function getBizContent()
    {
        if (!empty($this->bizContentarr)) {
            $this->bizContent = json_encode($this->bizContentarr, JSON_UNESCAPED_UNICODE);
        }
        return $this->bizContent;
    }

    /**
     * @param string $appAuthToken
     */
    public function setAppAuthToken(string $appAuthToken): void
    {
        $this->appAuthToken = $appAuthToken;
        $this->bizContentarr['app_auth_token'] = $appAuthToken;
    }

    /**
     * @return string
     */
    public function getAppAuthToken(): string
    {
        return $this->appAuthToken;
    }
}