<?php
/**
 * Created by PhpStorm.
 * User: wuliangbo
 * Date: 2020/1/9
 * Time: 15:37
 */

namespace EasyAlipay\OpenPlatform\Model;

use EasyAlipay\Payment\Model\BaseContentBuilder;

class AlipayOpenAgentCancelContentBuilder extends BaseContentBuilder
{
    /**
     * 事务编号
     * @var string
     */
    protected $batchNo;

    /**
     * 设置事务编号
     * @param $batchNo
     */
    public function setBatchNo($batchNo)
    {
        $this->batchNo = $batchNo;
        $this->bizContentarr['batch_no'] = $batchNo;
    }

    /**
     * 获取事务编号
     * @return string
     */
    public function getBatchNo()
    {
        return $this->batchNo;
    }
}