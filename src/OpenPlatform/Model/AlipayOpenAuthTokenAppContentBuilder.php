<?php
/**
 * Created by PhpStorm.
 * User: wuliangbo
 * Date: 2020/1/9
 * Time: 10:27
 */

namespace EasyAlipay\OpenPlatform\Model;

use EasyAlipay\Payment\Model\BaseContentBuilder;

class AlipayOpenAuthTokenAppContentBuilder extends BaseContentBuilder
{
    /**
     * 获取token方式 authorization_code｜refresh_token
     * @var string
     */
    protected $grantType;

    /**
     * 授权码
     * @var string
     */
    protected $code;

    /**
     * 刷新令牌
     * @var string
     */
    protected $refreshToken;

    /**
     * 设置token获取方式
     * @param $grantType
     */
    public function setGrantType($grantType)
    {
        $this->grantType = $grantType;
        $this->bizContentarr['grant_type'] = $grantType;
    }

    /**
     * 获取token获取方式
     * @return string
     */
    public function getGrantType()
    {
        return $this->grantType;
    }

    /**
     * 设置授权码
     * @param $code
     */
    public function setCode($code)
    {
        $this->code = $code;
        $this->bizContentarr['code'] = $code;
    }

    /**
     * 获取授权码
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * 设置刷新令牌
     * @param $refreshToken
     */
    public function setRefreshToken($refreshToken)
    {
        $this->refreshToken = $refreshToken;
        $this->bizContentarr['refresh_token'] = $refreshToken;
    }

    /**
     * 获取令牌
     * @return string
     */
    public function getRefreshToken()
    {
        return $this->refreshToken;
    }
}