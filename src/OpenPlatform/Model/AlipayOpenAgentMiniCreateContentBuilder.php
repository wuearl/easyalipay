<?php
/**
 * Created by PhpStorm.
 * User: wuliangbo
 * Date: 2020/1/9
 * Time: 15:08
 */

namespace EasyAlipay\OpenPlatform\Model;

use EasyAlipay\Kernel\AppRequest;

class AlipayOpenAgentMiniCreateContentBuilder extends AppRequest
{
    /**
     * 小程序应用类目，参数格式：一级类目_二级类目。
     * 应用类目参考文档：https://docs.alipay.com/isv/10325
     * @var string
     */
    private $appCategoryIds;

    /**
     * 商家小程序描述信息，简要描述小程序主要功能（20-200个字），例：xx小程序提供了xx功能，主要解决了XX问题。
     * @var string
     */
    private $appDesc;

    /**
     * 小程序英文名称，长度3~20个字符
     * @var string
     */
    private $appEnglishName;

    /**
     * 商家小程序应用图标，最大256KB，LOGO不允许涉及政治敏感与色情；图片格式必须为：png、jpeg、jpg，建议上传像素为180*180，LOGO核心图形建议在白色160PX范围内
     * @var string
     */
    private $appLogo;

    /**
     * 代商家创建的小程序应用名称。名称可以由中文、数字、英文及下划线组成，长度在3-20个字符之间，一个中文字等于2个字符，更多名称规则见：https://docs.alipay.com/mini/operation/name
     * @var string
     */
    private $appName;

    /**
     * 代商家创建的小程序的简介，请用一句话简要描述小程序提供的服务；应用上架后一个自然月可修改5次（10~32个字符）
     * @var string
     */
    private $appSlogan;

    /**
     * @var string
     */
    private $batchNo;

    /**
     * 商家小程序客服邮箱
     * 商家小程序客服电话和邮箱，可以二选一填写，但不能同时为空
     * @var string
     */
    private $serviceEmail;

    /**
     * 商家小程序的客服电话，推荐填写
     * 商家小程序客服电话和邮箱，可以二选一填写，但不能同时为空
     * @var string
     */
    private $servicePhone;

    /**
     * 设置小程序应用类目
     * @param $appCategoryIds
     */
    public function setAppCategoryIds($appCategoryIds)
    {
        $this->appCategoryIds = $appCategoryIds;
        $this->apiParas["app_category_ids"] = $appCategoryIds;
    }

    /**
     * 获取小程序应用类目
     * @return string
     */
    public function getAppCategoryIds()
    {
        return $this->appCategoryIds;
    }

    /**
     * 设置小程序描述信息
     * @param $appDesc
     */
    public function setAppDesc($appDesc)
    {
        $this->appDesc = $appDesc;
        $this->apiParas["app_desc"] = $appDesc;
    }

    /**
     * 获取小程序描述信息
     * @return string
     */
    public function getAppDesc()
    {
        return $this->appDesc;
    }

    /**
     * 设置小程序英文名称
     * @param $appEnglishName
     */
    public function setAppEnglishName($appEnglishName)
    {
        $this->appEnglishName = $appEnglishName;
        $this->apiParas["app_english_name"] = $appEnglishName;
    }

    /**
     * 获取小程序英文名称
     * @return string
     */
    public function getAppEnglishName()
    {
        return $this->appEnglishName;
    }

    /**
     * 设置小程序应用图标
     * @param $appLogo
     */
    public function setAppLogo($appLogo)
    {
        $this->appLogo = $appLogo;
        $this->apiParas["app_logo"] = $appLogo;
    }

    /**
     * 获取小程序应用图标
     * @return string
     */
    public function getAppLogo()
    {
        return $this->appLogo;
    }

    /**
     * 设置小程序应用名称
     * @param $appName
     */
    public function setAppName($appName)
    {
        $this->appName = $appName;
        $this->apiParas["app_name"] = $appName;
    }

    /**
     * 获取小程序应用名称
     * @return string
     */
    public function getAppName()
    {
        return $this->appName;
    }

    /**
     * 设置小程序简介
     * @param $appSlogan
     */
    public function setAppSlogan($appSlogan)
    {
        $this->appSlogan = $appSlogan;
        $this->apiParas["app_slogan"] = $appSlogan;
    }

    /**
     * 获取小程序简介
     * @return string
     */
    public function getAppSlogan()
    {
        return $this->appSlogan;
    }

    /**
     * 设置事务编号
     * @param $batchNo
     */
    public function setBatchNo($batchNo)
    {
        $this->batchNo = $batchNo;
        $this->apiParas["batch_no"] = $batchNo;
    }

    /**
     * 获取事务编号
     * @return string
     */
    public function getBatchNo()
    {
        return $this->batchNo;
    }

    /**
     * 设置客服邮箱
     * @param $serviceEmail
     */
    public function setServiceEmail($serviceEmail)
    {
        $this->serviceEmail = $serviceEmail;
        $this->apiParas["service_email"] = $serviceEmail;
    }

    /**
     * 获取客服邮箱
     * @return string
     */
    public function getServiceEmail()
    {
        return $this->serviceEmail;
    }

    /**
     * 设置客服电话
     * @param $servicePhone
     */
    public function setServicePhone($servicePhone)
    {
        $this->servicePhone = $servicePhone;
        $this->apiParas["service_phone"] = $servicePhone;
    }

    /**
     * 获取客服电话
     * @return string
     */
    public function getServicePhone()
    {
        return $this->servicePhone;
    }
}