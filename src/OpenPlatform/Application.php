<?php

namespace EasyAlipay\OpenPlatform;

use EasyAlipay\Kernel\ServiceContainer;
use EasyAlipay\OpenPlatform\Authorizer\Mini\Application as Mini;
use EasyAlipay\OpenPlatform\Authorizer\OpenPublic\Application as OpenPublic;

/**
 * Class Application
 * @package EasyAlipay\OpenPlatform
 * @property \EasyAlipay\OpenPlatform\Server\Guard   $server
 *
 * @method mixed getToken(string $grant_type, string $code, string $refresh_token = '')
 * @method mixed redirect(string $redirect_uri, string $state, array $type = ['TINYAPP'])
 * @method mixed queryAppAuth(string $appAuthToken)
 */
class Application extends ServiceContainer
{
    /**
     * @var array
     */
    protected $providers = [
        Base\ServiceProvider::class,
        Server\ServiceProvider::class
    ];

    /**
     * @param string $appId
     * @param string|null $appAuthToken
     * @return Mini
     */
    public function mini(string $appId, string $appAuthToken = null): Mini
    {
        $application = new Mini($this->getAuthorizerConfig($appId, $appAuthToken), $this->getReplaceServices());
        return $application;
    }

    /**
     * @param string $appId
     * @param string|null $appAuthToken
     * @return OpenPublic
     */
    public function openPublic(string $appId, string $appAuthToken = null): OpenPublic
    {
        $application = new OpenPublic($this->getAuthorizerConfig($appId, $appAuthToken), $this->getReplaceServices());
        return $application;
    }

    /**
     * @param string $appId
     * @param string|null $appAuthToken
     * @return array
     */
    protected function getAuthorizerConfig(string $appId, string $appAuthToken = null): array
    {
        return $this['config']->merge([
            'auth_app_id' => $appId,
            'app_auth_token' => $appAuthToken,
        ])->toArray();
    }

    /**
     * @return array
     */
    protected function getReplaceServices(): array
    {
        $services = [];

        foreach (['cache', 'http_client', 'log', 'logger', 'request'] as $reuse) {
            if (isset($this[$reuse])) {
                $services[$reuse] = $this[$reuse];
            }
        }
        return $services;
    }

    /**
     * Handle dynamic calls.
     * @param $method
     * @param $args
     * @return mixed
     */
    public function __call($method, $args)
    {
        return $this->base->$method(...$args);
    }
}
