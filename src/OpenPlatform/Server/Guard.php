<?php
/**
 * Created by PhpStorm.
 * User: wuliangbo
 * Date: 2020/1/8
 * Time: 15:25
 */

namespace EasyAlipay\OpenPlatform\Server;

use EasyAlipay\Kernel\ServerGuard;
use Symfony\Component\HttpFoundation\Response;
use function EasyAlipay\Kernel\data_get;

class Guard extends ServerGuard
{
    protected function resolve(): Response
    {
        $this->registerHandlers();

        $message = $this->getMessage();


        return new Response(static::SUCCESS_EMPTY_RESPONSE);
    }

    /**
     * Register event handlers.
     */
    protected function registerHandlers()
    {

    }
}