<?php
/**
 * Created by PhpStorm.
 * User: wuliangbo
 * Date: 2020/1/8
 * Time: 14:30
 */

namespace EasyAlipay\OpenPlatform\Server;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ServiceProvider implements ServiceProviderInterface
{
    /**
     * @param Container $app
     */
    public function register(Container $app)
    {
        $app['server'] = function ($app) {
            return new Guard($app);
        };
    }
}