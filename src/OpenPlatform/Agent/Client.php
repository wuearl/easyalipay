<?php
/**
 * Created by PhpStorm.
 * User: wuliangbo
 * Date: 2020/1/8
 * Time: 15:25
 */

namespace EasyAlipay\OpenPlatform\Agent;

use EasyAlipay\Kernel\AppClient;
use EasyAlipay\Kernel\AppRequest;
use EasyAlipay\OpenPlatform\Model\AlipayOpenAgentCancelContentBuilder;
use EasyAlipay\OpenPlatform\Model\AlipayOpenAgentConfirmContentBuilder;
use EasyAlipay\OpenPlatform\Model\AlipayOpenAgentCreateContentBuilder;
use EasyAlipay\OpenPlatform\Model\AlipayOpenAgentMiniCreateContentBuilder;
use EasyAlipay\OpenPlatform\Model\AlipayOpenAgentOrderQueryContentBuilder;

class Client extends AppClient
{
    /**
     * 开启代商户签约、创建应用事务
     * @param string $account
     * @param array $contact_info
     * @param string $order_ticket
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function create(string $account, array $contact_info, string $order_ticket)
    {
        $contentBuilder = new AlipayOpenAgentCreateContentBuilder();
        $contentBuilder->setAccount($account);
        $contentBuilder->setContactInfo($contact_info);
        $contentBuilder->setOrderTicket($order_ticket);
        $request = new AppRequest();
        $request->setBizContent($contentBuilder->getBizContent());
        $request->setApiMethodName('alipay.open.agent.create');
        return ($this->execute($request));
    }

    /**
     * 代商家创建小程序应用
     * @param array $payload
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function miniCreate(array $payload)
    {
        $request = new AlipayOpenAgentMiniCreateContentBuilder();
        $request->setAppName($payload['app_name']);
        $request->setAppEnglishName($payload['app_english_name']);
        $request->setAppCategoryIds($payload['app_category_ids']);
        $request->setAppDesc($payload['app_desc']);
        $request->setAppLogo($payload['app_logo']);
        $request->setAppSlogan($payload['app_slogan']);
        if (isset($payload['service_email'])) {
            $request->setServiceEmail($payload['service_email']);
        }
        if (isset($payload['service_phone'])) {
            $request->setServiceEmail($payload['service_phone']);
        }
        $request->setBatchNo($payload['batch_no']);
        $request->setApiMethodName('alipay.open.agent.mini.create');
        return ($this->execute($request));
    }

    /**
     * 提交代商户签约、创建应用事务
     * @param string $batch_no
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function confirm(string $batchNo)
    {
        $contentBuilder = new AlipayOpenAgentConfirmContentBuilder();
        $contentBuilder->setBatchNo($batchNo);
        $request = new AppRequest();
        $request->setBizContent($contentBuilder->getBizContent());
        $request->setApiMethodName('alipay.open.agent.confirm');
        return ($this->execute($request));
    }

    /**
     * 查询申请单状态
     * @param string $batch_no
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function query(string $batchNo)
    {
        $contentBuilder = new AlipayOpenAgentOrderQueryContentBuilder();
        $contentBuilder->setBatchNo($batchNo);
        $request = new AppRequest();
        $request->setBizContent($contentBuilder->getBizContent());
        $request->setApiMethodName('alipay.open.agent.order.query');
        return ($this->execute($request));
    }

    /**
     * 取消代商户签约、创建应用事务
     * @param string $batch_no
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function cancel(string $batchNo)
    {
        $contentBuilder = new AlipayOpenAgentCancelContentBuilder();
        $contentBuilder->setBatchNo($batchNo);
        $request = new AppRequest();
        $request->setBizContent($contentBuilder->getBizContent());
        $request->setApiMethodName('alipay.open.agent.cancel');
        return ($this->execute($request));
    }
}