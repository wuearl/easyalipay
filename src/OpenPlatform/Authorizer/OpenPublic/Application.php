<?php
/**
 * Created by PhpStorm.
 * User: wuliangbo
 * Date: 2020/1/8
 * Time: 14:35
 */

namespace EasyAlipay\OpenPlatform\Authorizer\OpenPublic;

use EasyAlipay\OpenPublic\Application as OpenPublic;

class Application extends OpenPublic
{
    public function __construct(array $config = [], array $prepends = [])
    {
        parent::__construct($config, $prepends);
        $providers = [
        ];

        foreach ($providers as $provider) {
            $this->register(new $provider());
        }
    }
}