<?php
/**
 * Created by PhpStorm.
 * User: wuliangbo
 * Date: 2020/3/13
 * Time: 11:53
 */

namespace EasyAlipay\OpenPlatform\Authorizer\Mini\Experience;

use EasyAlipay\Kernel\AppClient;
use EasyAlipay\Kernel\AppRequest;
use EasyAlipay\OpenPlatform\Authorizer\Mini\Model\ExperienceCancelContentBuilder;
use EasyAlipay\OpenPlatform\Authorizer\Mini\Model\ExperienceCreateContentBuilder;
use EasyAlipay\OpenPlatform\Authorizer\Mini\Model\ExperienceQueryContentBuilder;

class Client extends AppClient
{
    /**
     * 生成小程序体验版
     * @param string $appVersion
     * @param string $bundleId
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function create(string $appVersion, string $bundleId = '')
    {
        $contentBuilder = new ExperienceCreateContentBuilder();
        $contentBuilder->setAppVersion($appVersion);
        if (!empty($bundleId)) {
            $contentBuilder->setBundleId($bundleId);
        }
        $request = new AppRequest();
        $request->setBizContent($contentBuilder->getBizContent());
        $request->setApiMethodName('alipay.open.mini.experience.create');
        return ($this->execute($request));
    }

    /**
     * 查询小程序体验版 返回二维码
     * @param string $appVersion
     * @param string $bundleId
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function query(string $appVersion, string $bundleId = '')
    {
        $contentBuilder = new ExperienceQueryContentBuilder();
        $contentBuilder->setAppVersion($appVersion);
        if (!empty($bundleId)) {
            $contentBuilder->setBundleId($bundleId);
        }
        $request = new AppRequest();
        $request->setBizContent($contentBuilder->getBizContent());
        $request->setApiMethodName('alipay.open.mini.experience.query');
        return ($this->execute($request));
    }

    /**
     * 取消体验版
     * @param string $appVersion
     * @param string $bundleId
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function cancel(string $appVersion, string $bundleId = '')
    {
        $contentBuilder = new ExperienceCancelContentBuilder();
        $contentBuilder->setAppVersion($appVersion);
        if (!empty($bundleId)) {
            $contentBuilder->setBundleId($bundleId);
        }
        $request = new AppRequest();
        $request->setBizContent($contentBuilder->getBizContent());
        $request->setApiMethodName('alipay.open.mini.experience.cancel');
        return ($this->execute($request));
    }
}