<?php
/**
 * Created by PhpStorm.
 * User: wuliangbo
 * Date: 2020/3/13
 * Time: 11:51
 */
namespace EasyAlipay\OpenPlatform\Authorizer\Mini\BaseInfo;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['base_info'] = function ($app) {
            return new Client($app);
        };
    }
}