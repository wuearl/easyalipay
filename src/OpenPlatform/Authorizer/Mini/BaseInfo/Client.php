<?php
/**
 * Created by PhpStorm.
 * User: wuliangbo
 * Date: 2020/3/13
 * Time: 11:53
 */

namespace EasyAlipay\OpenPlatform\Authorizer\Mini\BaseInfo;

use EasyAlipay\Kernel\AppClient;
use EasyAlipay\Kernel\AppRequest;
use EasyAlipay\OpenPlatform\Authorizer\Mini\Model\BaseModifyContentBuilder;
use EasyAlipay\OpenPlatform\Authorizer\Mini\Model\BaseQueryContentBuilder;

class Client extends AppClient
{
    /**
     * 查询小程序基础信息
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function query()
    {
        $contentBuilder = new BaseQueryContentBuilder();
        $request = new AppRequest();
        $request->setBizContent($contentBuilder->getBizContent());
        $request->setApiMethodName('alipay.open.mini.baseinfo.query');
        return ($this->execute($request));
    }

    /**
     * 修改小程序基础信息
     * @param array $params
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function modify(array $params)
    {
        $contentBuilder = new BaseModifyContentBuilder();
        $contentBuilder->setParams($params);
        $request = new AppRequest();
        $request->setBizContent($contentBuilder->getBizContent());
        $request->setApiMethodName('alipay.open.mini.baseinfo.modify');
        return ($this->execute($request));
    }
}