<?php
/**
 * Created by PhpStorm.
 * User: wuliangbo
 * Date: 2020/3/13
 * Time: 11:53
 */

namespace EasyAlipay\OpenPlatform\Authorizer\Mini\Version;

use EasyAlipay\Kernel\AppClient;
use EasyAlipay\Kernel\AppRequest;
use EasyAlipay\OpenPlatform\Authorizer\Mini\Model\VersionAuditContentBuilder;
use EasyAlipay\OpenPlatform\Authorizer\Mini\Model\VersionCancelContentBuilder;
use EasyAlipay\OpenPlatform\Authorizer\Mini\Model\VersionOfflineContentBuilder;
use EasyAlipay\OpenPlatform\Authorizer\Mini\Model\VersionOnlineContentBuilder;
use EasyAlipay\OpenPlatform\Authorizer\Mini\Model\VersionRollbackContentBuilder;
use EasyAlipay\OpenPlatform\Authorizer\Mini\Model\VersionUploadContentBuilder;

class Client extends AppClient
{
    /**
     * 小程序版本列表
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function list()
    {
        $request = new AppRequest();
        $request->setApiMethodName("alipay.open.mini.version.list.query");
        return ($this->execute($request));
    }

    /**
     * 小程序版本详情
     * @param string $appVersion
     * @param string $bundleId
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function query(string $appVersion, string $bundleId = '')
    {
        $contentBuilder = new VersionOnlineContentBuilder();
        $contentBuilder->setAppVersion($appVersion);
        if (!empty($bundleId)) {
            $contentBuilder->setBundleId($bundleId);
        }
        $request = new AppRequest();
        $request->setBizContent($contentBuilder->getBizContent());
        $request->setApiMethodName("alipay.open.mini.version.detail.query");
        return ($this->execute($request));
    }

    /**
     * 上传构建小程序
     * @param array $payload
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function upload(array $payload)
    {
        $contentBuilder = new VersionUploadContentBuilder();
        $contentBuilder->setTemplateId($payload['template_id']);
        $contentBuilder->setAppVersion($payload['app_version']);
        $contentBuilder->setExt($payload['ext']);
        if (!empty($payload['template_version'])) {
            $contentBuilder->setTemplateVersion($payload['template_version']);
        }
        if (!empty($payload['bundle_id'])) {
            $contentBuilder->setBundleId($payload['bundle_id']);
        }
        $request = new AppRequest();
        $request->setBizContent($contentBuilder->getBizContent());
        $request->setApiMethodName("alipay.open.mini.version.upload");
        return ($this->execute($request));
    }

    /**
     * 提交审核
     * @param string $appVersion
     * @param string $versionDesc
     * @param string $regionType
     * @param array $serviceRegionInfo
     * @param array $params
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function audit(string $appVersion, string $versionDesc, string $regionType, array $serviceRegionInfo = [], array $params = [])
    {
        $request = new VersionAuditContentBuilder();
        $request->setAppVersion($appVersion);
        $request->setVersionDesc($versionDesc);
        $request->setRegionType($regionType);
        if ($regionType == 'LOCATION') {
            $request->setServiceRegionInfo($serviceRegionInfo);
        }
        $request->setParams($params);
        $request->setApiMethodName("alipay.open.mini.version.audit.apply");
        return ($this->execute($request));
    }

    /**
     * 上架小程序
     * @param string $appVersion
     * @param string $bundleId
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function online(string $appVersion, string $bundleId = '')
    {
        $contentBuilder = new VersionOnlineContentBuilder();
        $contentBuilder->setAppVersion($appVersion);
        if (!empty($bundleId)) {
            $contentBuilder->setBundleId($bundleId);
        }
        $request = new AppRequest();
        $request->setBizContent($contentBuilder->getBizContent());
        $request->setApiMethodName("alipay.open.mini.version.online");
        return ($this->execute($request));
    }

    /**
     * 下架小程序
     * @param string $appVersion
     * @param string $bundleId
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function offline(string $appVersion, string $bundleId = '')
    {
        $contentBuilder = new VersionOfflineContentBuilder();
        $contentBuilder->setAppVersion($appVersion);
        if (!empty($bundleId)) {
            $contentBuilder->setBundleId($bundleId);
        }
        $request = new AppRequest();
        $request->setBizContent($contentBuilder->getBizContent());
        $request->setApiMethodName("alipay.open.mini.version.offline");
        return ($this->execute($request));
    }

    /**
     * 退回开发
     * @param string $appVersion
     * @param string $bundleId
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function cancel(string $appVersion, string $bundleId = '')
    {
        $contentBuilder = new VersionCancelContentBuilder();
        $contentBuilder->setAppVersion($appVersion);
        if (!empty($bundleId)) {
            $contentBuilder->setBundleId($bundleId);
        }
        $request = new AppRequest();
        $request->setBizContent($contentBuilder->getBizContent());
        $request->setApiMethodName("alipay.open.mini.version.audited.cancel");
        return ($this->execute($request));
    }

    /**
     * 撤销审核
     * @param string $appVersion
     * @param string $bundleId
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function auditCancel(string $appVersion, string $bundleId = '')
    {
        $contentBuilder = new VersionCancelContentBuilder();
        $contentBuilder->setAppVersion($appVersion);
        if (!empty($bundleId)) {
            $contentBuilder->setBundleId($bundleId);
        }
        $request = new AppRequest();
        $request->setBizContent($contentBuilder->getBizContent());
        $request->setApiMethodName("alipay.open.mini.version.audit.cancel");
        return ($this->execute($request));
    }

    /**
     * 回滚小程序
     * @param string $appVersion
     * @param string $bundleId
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function rollback(string $appVersion, string $bundleId = '')
    {
        $contentBuilder = new VersionRollbackContentBuilder();
        $contentBuilder->setAppVersion($appVersion);
        if (!empty($bundleId)) {
            $contentBuilder->setBundleId($bundleId);
        }
        $request = new AppRequest();
        $request->setBizContent($contentBuilder->getBizContent());
        $request->setApiMethodName("alipay.open.mini.version.rollback");
        return ($this->execute($request));
    }

    /**
     * 小程序删除版本
     * @param string $appVersion
     * @param string $bundleId
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function delete(string $appVersion, string $bundleId = '')
    {
        $contentBuilder = new VersionRollbackContentBuilder();
        $contentBuilder->setAppVersion($appVersion);
        if (!empty($bundleId)) {
            $contentBuilder->setBundleId($bundleId);
        }
        $request = new AppRequest();
        $request->setBizContent($contentBuilder->getBizContent());
        $request->setApiMethodName("alipay.open.mini.version.delete");
        return ($this->execute($request));
    }
}