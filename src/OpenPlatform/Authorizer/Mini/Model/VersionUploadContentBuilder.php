<?php
/**
 * Created by PhpStorm.
 * User: wuliangbo
 * Date: 2020/3/13
 * Time: 14:30
 */

namespace EasyAlipay\OpenPlatform\Authorizer\Mini\Model;
class VersionUploadContentBuilder
{
    /**
     * @var string 模板版本号
     */
    private $templateVersion;
    /**
     * @var string 模板的配置参数
     */
    private $ext;
    /**
     * @var string 模板id
     */
    private $templateId;
    /**
     * @var string 小程序版本号
     */
    private $appVersion;
    /**
     * @var string 小程序投放的端参数 com.alipay.alipaywallet:支付宝端
     */
    private $bundleId;
    /**
     * @var array 参数数组
     */
    private $bizContentarr = [];
    /**
     * @var string  参数字符串
     */
    private $bizContent = NULL;

    public function getBizContent()
    {
        if (!empty($this->bizContentarr)) {
            $this->bizContent = json_encode($this->bizContentarr, JSON_UNESCAPED_UNICODE);
        }
        return $this->bizContent;
    }

    /**
     * @param $templateVersion
     */
    public function setTemplateVersion($templateVersion)
    {
        $this->templateVersion = $templateVersion;
        $this->bizContentarr['template_version'] = $templateVersion;
    }

    /**
     * @return string
     */
    public function getTemplateVersion()
    {
        return $this->templateVersion;
    }

    /**
     * @param $ext
     */
    public function setExt($ext)
    {
        $this->ext = $ext;
        $this->bizContentarr['ext'] = $ext;
    }

    /**
     * @return string
     */
    public function getExt()
    {
        return $this->ext;
    }

    /**
     * @param $templateId
     */
    public function setTemplateId($templateId)
    {
        $this->templateId = $templateId;
        $this->bizContentarr['template_id'] = $templateId;
    }

    /**
     * @return string
     */
    public function getTemplateId()
    {
        return $this->templateId;
    }

    /**
     * @param $appVersion
     */
    public function setAppVersion($appVersion)
    {
        $this->appVersion = $appVersion;
        $this->bizContentarr['app_version'] = $appVersion;
    }

    /**
     * @return string
     */
    public function getAppVersion()
    {
        return $this->appVersion;
    }

    /**
     * @param $bundleId
     */
    public function setBundleId($bundleId)
    {
        $this->bundleId = $bundleId;
        $this->bizContentarr['bundle_id'] = $bundleId;
    }

    /**
     * @return string
     */
    public function getBundleId()
    {
        return $this->bundleId;
    }
}