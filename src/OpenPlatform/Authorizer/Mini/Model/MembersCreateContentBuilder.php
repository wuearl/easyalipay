<?php
/**
 * Created by PhpStorm.
 * User: wuliangbo
 * Date: 2020/3/13
 * Time: 15:24
 */

namespace EasyAlipay\OpenPlatform\Authorizer\Mini\Model;
class MembersCreateContentBuilder
{
    /**
     * @var string 支付宝登录账号ID
     */
    protected $logonId;
    /**
     * @var string 成员的角色类型，DEVELOPER-开发者|EXPERIENCER-体验者
     */
    protected $role;
    /**
     * @var string 蚂蚁统一会员ID
     */
    protected $userId;
    /**
     * @var array 参数数组
     */

    protected $bizContentarr = [];
    /**
     * @var string  参数字符串
     */
    protected $bizContent = NULL;

    public function getBizContent()
    {
        if (!empty($this->bizContentarr)) {
            $this->bizContent = json_encode($this->bizContentarr, JSON_UNESCAPED_UNICODE);
        }
        return $this->bizContent;
    }

    public function setLogonId($logonId)
    {
        $this->logonId = $logonId;
        $this->bizContentarr['logon_id'] = $logonId;
    }

    public function getLogonId()
    {
        return $this->logonId;
    }

    public function setRole($role)
    {
        $this->role = $role;
        $this->bizContentarr['role'] = $role;
    }

    public function getRole(): string
    {
        return $this->role;
    }

    /**
     * @param string $userId
     */
    public function setUserId(string $userId): void
    {
        $this->userId = $userId;
        $this->bizContentarr['user_id'] = $userId;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }
}