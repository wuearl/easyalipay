<?php
/**
 * Created by PhpStorm.
 * User: wuliangbo
 * Date: 2020/3/13
 * Time: 16:06
 */
namespace EasyAlipay\OpenPlatform\Authorizer\Mini\Model;
class DomainCreateContentBuilder
{
    /**
     * @var string httpRequest域白名单
     */
    protected $safeDomain;
    /**
     * @var array 参数数组
     */
    protected $bizContentarr = [];
    /**
     * @var string  参数字符串
     */
    protected $bizContent = NULL;

    public function getBizContent()
    {
        if (!empty($this->bizContentarr)) {
            $this->bizContent = json_encode($this->bizContentarr, JSON_UNESCAPED_UNICODE);
        }
        return $this->bizContent;
    }
    /**
     * @return mixed
     */
    public function getSafeDomain()
    {
        return $this->safeDomain;
    }

    /**
     * @param mixed $safeDomain
     */
    public function setSafeDomain($safeDomain): void
    {
        $this->safeDomain = $safeDomain;
    }
}