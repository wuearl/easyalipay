<?php
/**
 * Created by PhpStorm.
 * User: wuliangbo
 * Date: 2020/3/13
 * Time: 14:30
 */

namespace EasyAlipay\OpenPlatform\Authorizer\Mini\Model;
use EasyAlipay\Kernel\AppRequest;

class VersionAuditContentBuilder extends AppRequest
{
    /**
     * @var string 小程序版本号
     */
    private $appVersion;
    /**
     * @var string 小程序版本描述，30-500个字符
     */
    private $versionDesc;
    /**
     * @var string 小程序服务区域类型，GLOBAL-全球，CHINA-中国，LOCATION-指定区域
     */
    private $regionType;
    /**
     * @var array 省市区信息，当区域类型为LOCATION时，不能为空
     */
    private $serviceRegionInfo;

    /**
     * @param $appVersion
     */
    public function setAppVersion($appVersion)
    {
        $this->appVersion = $appVersion;
        $this->apiParas['app_version'] = $appVersion;
    }

    /**
     * @return string
     */
    public function getAppVersion()
    {
        return $this->appVersion;
    }

    /**
     * @param $versionDesc
     */
    public function setVersionDesc($versionDesc)
    {
        $this->versionDesc = $versionDesc;
        $this->apiParas['version_desc'] = $versionDesc;
    }

    /**
     * @return string
     */
    public function getVersionDesc()
    {
        return $this->versionDesc;
    }

    /**
     * @param $regionType
     */
    public function setRegionType($regionType)
    {
        $this->regionType = $regionType;
        $this->apiParas['region_type'] = $regionType;
    }

    /**
     * @return string
     */
    public function getRegionType()
    {
        return $this->regionType;
    }

    /**
     * @param $serviceRegionInfo
     */
    public function setServiceRegionInfo($serviceRegionInfo)
    {
        $this->serviceRegionInfo = $serviceRegionInfo;
        $this->apiParas['service_region_info'] = $serviceRegionInfo;
    }

    /**
     * @return array
     */
    public function getServiceRegionInfo()
    {
        return $this->serviceRegionInfo;
    }

    /**
     * 设置非必填参数
     * @param $params
     */
    public function setParams($params)
    {
        foreach ($params as $k => $v) {
            $this->apiParas[$k] = $v;
        }
    }
}