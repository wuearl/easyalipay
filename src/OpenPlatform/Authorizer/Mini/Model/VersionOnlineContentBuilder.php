<?php
/**
 * Created by PhpStorm.
 * User: wuliangbo
 * Date: 2020/3/13
 * Time: 14:30
 */

namespace EasyAlipay\OpenPlatform\Authorizer\Mini\Model;
class VersionOnlineContentBuilder
{
    /**
     * @var string 小程序版本号
     */
    protected $appVersion;
    /**
     * @var string 小程序投放的端参数 com.alipay.alipaywallet:支付宝端
     */
    protected $bundleId;
    /**
     * @var array 参数数组
     */
    protected $bizContentarr = [];
    /**
     * @var string  参数字符串
     */
    protected $bizContent = NULL;

    public function getBizContent()
    {
        if (!empty($this->bizContentarr)) {
            $this->bizContent = json_encode($this->bizContentarr, JSON_UNESCAPED_UNICODE);
        }
        return $this->bizContent;
    }

    /**
     * @param $appVersion
     */
    public function setAppVersion($appVersion)
    {
        $this->appVersion = $appVersion;
        $this->bizContentarr['app_version'] = $appVersion;
    }

    /**
     * @return string
     */
    public function getAppVersion()
    {
        return $this->appVersion;
    }

    /**
     * @param $bundleId
     */
    public function setBundleId($bundleId)
    {
        $this->bundleId = $bundleId;
        $this->bizContentarr['bundle_id'] = $bundleId;
    }

    /**
     * @return string
     */
    public function getBundleId()
    {
        return $this->bundleId;
    }
}