<?php
/**
 * Created by PhpStorm.
 * User: wuliangbo
 * Date: 2020/3/13
 * Time: 15:51
 */

namespace EasyAlipay\OpenPlatform\Authorizer\Mini\Model;
class BaseQueryContentBuilder
{
    /**
     * @var array 参数数组
     */
    protected $bizContentarr = [];
    /**
     * @var string  参数字符串
     */
    protected $bizContent = NULL;

    public function getBizContent()
    {
        if (!empty($this->bizContentarr)) {
            $this->bizContent = json_encode($this->bizContentarr, JSON_UNESCAPED_UNICODE);
        }
        return $this->bizContent;
    }
}