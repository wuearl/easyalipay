<?php
/**
 * Created by PhpStorm.
 * User: wuliangbo
 * Date: 2020/4/14
 * Time: 15:58
 */
namespace EasyAlipay\OpenPlatform\Authorizer\Mini\Model;
class CategoryQueryContentBuilder extends BaseQueryContentBuilder
{
    /**
     * @var bool 是否需要过滤不可用类目
     */
    protected $isFilter;

    /**
     * @return mixed
     */
    public function getIsFilter()
    {
        return $this->isFilter;
    }

    /**
     * @param mixed $isFilter
     */
    public function setIsFilter($isFilter): void
    {
        $this->isFilter = $isFilter;
        $this->bizContentarr['is_filter'] = $isFilter;
    }
}