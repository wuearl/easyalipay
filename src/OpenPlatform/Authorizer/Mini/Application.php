<?php
/**
 * Created by PhpStorm.
 * User: wuliangbo
 * Date: 2020/1/8
 * Time: 14:37
 */

namespace EasyAlipay\OpenPlatform\Authorizer\Mini;

use EasyAlipay\Mini\Application as Mini;

/**
 * Class Application
 * @package EasyAlipay\OpenPlatform\Authorizer\Mini
 *
 * @property \EasyAlipay\OpenPlatform\Authorizer\Mini\Version\Client     $version
 * @property \EasyAlipay\OpenPlatform\Authorizer\Mini\Domain\Client      $domain
 * @property \EasyAlipay\OpenPlatform\Authorizer\Mini\Experience\Client  $experience
 * @property \EasyAlipay\OpenPlatform\Authorizer\Mini\Members\Client     $members
 * @property \EasyAlipay\OpenPlatform\Authorizer\Mini\BaseInfo\Client    $base_info
 * @property \EasyAlipay\OpenPlatform\Authorizer\Mini\Category\Client    $category
 * @property \EasyAlipay\Base\Oauth\Client                               $oauth
 */
class Application extends Mini
{
    /**
     * Application constructor.
     * @param array $config
     * @param array $prepends
     */
    public function __construct(array $config = [], array $prepends = [])
    {
        parent::__construct($config, $prepends);
        $providers = [
            Version\ServiceProvider::class,
            Domain\ServiceProvider::class,
            Experience\ServiceProvider::class,
            Members\ServiceProvider::class,
            BaseInfo\ServiceProvider::class,
            Category\ServiceProvider::class,
            \EasyAlipay\Base\Oauth\ServiceProvider::class,
        ];

        foreach ($providers as $provider) {
            $this->register(new $provider());
        }
    }
}