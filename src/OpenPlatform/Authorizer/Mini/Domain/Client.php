<?php
/**
 * Created by PhpStorm.
 * User: wuliangbo
 * Date: 2020/3/13
 * Time: 11:53
 */

namespace EasyAlipay\OpenPlatform\Authorizer\Mini\Domain;

use EasyAlipay\Kernel\AppClient;
use EasyAlipay\Kernel\AppRequest;
use EasyAlipay\OpenPlatform\Authorizer\Mini\Model\DomainCreateContentBuilder;
use EasyAlipay\OpenPlatform\Authorizer\Mini\Model\DomainDeleteContentBuilder;

class Client extends AppClient
{
    /**
     * 添加域名白名单
     * @param string $safeDomain
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function create(string $safeDomain)
    {
        $contentBuilder = new DomainCreateContentBuilder();
        $contentBuilder->setSafeDomain($safeDomain);
        $request = new AppRequest();
        $request->setBizContent($contentBuilder->getBizContent());
        $request->setApiMethodName('alipay.open.mini.safedomain.create');
        return ($this->execute($request));
    }

    /**
     * 删除域名白名单
     * @param string $safeDomain
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function delete(string $safeDomain)
    {
        $contentBuilder = new DomainDeleteContentBuilder();
        $contentBuilder->setSafeDomain($safeDomain);
        $request = new AppRequest();
        $request->setBizContent($contentBuilder->getBizContent());
        $request->setApiMethodName('alipay.open.mini.safedomain.delete');
        return ($this->execute($request));
    }
}