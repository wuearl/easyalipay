<?php
/**
 * Created by PhpStorm.
 * User: wuliangbo
 * Date: 2020/3/13
 * Time: 11:53
 */

namespace EasyAlipay\OpenPlatform\Authorizer\Mini\Members;

use EasyAlipay\Kernel\AppClient;
use EasyAlipay\Kernel\AppRequest;
use EasyAlipay\OpenPlatform\Authorizer\Mini\Model\MembersCreateContentBuilder;
use EasyAlipay\OpenPlatform\Authorizer\Mini\Model\MembersDeleteContentBuilder;
use EasyAlipay\OpenPlatform\Authorizer\Mini\Model\MembersQueryContentBuilder;

class Client extends AppClient
{
    /**
     * 添加成员
     * @param $logonId
     * @param $role
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function create(string $logonId, string $role)
    {
        $contentBuilder = new MembersCreateContentBuilder();
        $contentBuilder->setLogonId($logonId);
        $contentBuilder->setRole($role);
        $request = new AppRequest();
        $request->setBizContent($contentBuilder->getBizContent());
        $request->setApiMethodName("alipay.open.app.members.create");
        return ($this->execute($request));
    }

    /**
     * 查询成员列表
     * @param $role
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function query(string $role)
    {
        $contentBuilder = new MembersQueryContentBuilder();
        $contentBuilder->setRole($role);
        $request = new AppRequest();
        $request->setBizContent($contentBuilder->getBizContent());
        $request->setApiMethodName("alipay.open.app.members.query");
        return ($this->execute($request));
    }

    /**
     * 删除成员
     * @param string $userId
     * @param string $role
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function delete(string $userId, string $role)
    {
        $contentBuilder = new MembersDeleteContentBuilder();
        $contentBuilder->setRole($role);
        $contentBuilder->setUserId($userId);
        $request = new AppRequest();
        $request->setBizContent($contentBuilder->getBizContent());
        $request->setApiMethodName("alipay.open.app.members.delete");
        return ($this->execute($request));
    }
}