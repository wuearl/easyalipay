<?php
/**
 * Created by PhpStorm.
 * User: wuliangbo
 * Date: 2020/3/13
 * Time: 11:51
 */
namespace EasyAlipay\OpenPlatform\Authorizer\Mini\Members;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['members'] = function ($app) {
            return new Client($app);
        };
    }
}