<?php
/**
 * Created by PhpStorm.
 * User: wuliangbo
 * Date: 2020/1/8
 * Time: 15:25
 */

namespace EasyAlipay\OpenPlatform\Base;

use EasyAlipay\Kernel\AppClient;
use EasyAlipay\Kernel\AppRequest;
use EasyAlipay\OpenPlatform\Model\AlipayOpenAuthTokenAppContentBuilder;
use EasyAlipay\OpenPlatform\Model\AuthTokenAppQueryContentBuilder;

class Client extends AppClient
{
    /**
     * 换取 auth_token
     * @param string $grant_type
     * @param string $code
     * @param string $refresh_token
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getToken(string $grant_type, string $code, string $refresh_token = '')
    {
        $contentBuilder = new AlipayOpenAuthTokenAppContentBuilder();
        $contentBuilder->setGrantType($grant_type);
        $contentBuilder->setCode($code);
        $contentBuilder->setRefreshToken($refresh_token);
        $request = new AppRequest();
        $request->setBizContent($contentBuilder->getBizContent());
        $request->setApiMethodName("alipay.open.auth.token.app");
        return ($this->execute($request));
    }
    /**
     * 授权地址
     * @param string $redirect_uri
     * @param string $state
     * @param array $type
     * @return string
     */
    public function redirect(string $redirectUri, string $state, array $type = ['TINYAPP'])
    {
        $base_url = 'https://openauth.alipay.com/oauth2/appToAppBatchAuth.htm';
        $parmas = [
            'app_id' => $this->config['app_id'],
            'redirect_uri' => $redirectUri,
            'state' => $state,
            'application_type' => implode(',', $type)
        ];
        $query = http_build_query($parmas);
        return $base_url . '?' . $query;
    }

    /**
     * 查询某个应用授权AppAuthToken的授权信息
     * @param $appAuthToken
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function queryAppAuth($appAuthToken)
    {
        $contentBuilder = new AuthTokenAppQueryContentBuilder();
        $contentBuilder->setAppAuthToken($appAuthToken);
        $request = new AppRequest();
        $request->setBizContent($contentBuilder->getBizContent());
        $request->setApiMethodName("alipay.open.auth.token.app.query");
        return ($this->execute($request));
    }
}