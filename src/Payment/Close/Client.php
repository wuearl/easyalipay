<?php

namespace EasyAlipay\Payment\Close;

use EasyAlipay\Kernel\AppClient;
use EasyAlipay\Kernel\AppRequest;
use EasyAlipay\Payment\Model\AlipayTradeCloseContentBuilder;

class Client extends AppClient
{
    /**
     * 关闭订单
     * @param string $out_trade_no
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function close(string $out_trade_no)
    {
        //构造查询业务请求参数对象
        $closeContentBuilder = new AlipayTradeCloseContentBuilder();
        $closeContentBuilder->setOutTradeNo($out_trade_no);
        $request = new AppRequest ();
        $request->setBizContent($closeContentBuilder->getBizContent());
        $request->setApiMethodName("alipay.trade.close");
        return($this->execute($request)) ;
    }
}
