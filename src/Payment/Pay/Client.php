<?php

namespace EasyAlipay\Payment\Pay;

use EasyAlipay\Kernel\AppClient;
use EasyAlipay\Kernel\AppRequest;
use EasyAlipay\Payment\Model\AlipayTradeAppPayContentBuilder;
use EasyAlipay\Payment\Model\AlipayTradeCreateContentBuilder;
use EasyAlipay\Payment\Model\AlipayTradePayContentBuilder;
use EasyAlipay\Payment\Model\AlipayTradePrecreateContentBuilder;
use EasyAlipay\Payment\Model\AlipayTradeWapPayContentBuilder;

class Client extends AppClient
{
    /**
     * 刷卡支付
     * @param array $payload
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function pos(array $payload)
    {
        $payContentBuilder = new AlipayTradePayContentBuilder();
        $payContentBuilder->setSubject($payload['subject']);
        $payContentBuilder->setOutTradeNo($payload['out_trade_no']);
        $payContentBuilder->setTotalAmount($payload['total_amount']);
        $payContentBuilder->setScene("bar_code");
        $payContentBuilder->setAuthCode($payload['auth_code']);
        if (isset($payload['body'])) {
            $payContentBuilder->setBody($payload['body']);
        }
        $payContentBuilder->setProductCode('FACE_TO_FACE_PAYMENT');
        $request = new AppRequest ();
        if (isset($payload['notify_url'])) {
            $request->setNotifyUrl($payload['notify_url']);
        }
        if (isset($payload['return_url'])) {
            $request->setReturnUrl($payload['return_url']);
        }
        $request->setBizContent($payContentBuilder->getBizContent());
        $request->setApiMethodName("alipay.trade.pay");
        return ($this->execute($request));
    }

    /**
     * @param array $payload
     * @return string
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     */
    public function app(array $payload)
    {
        $payContentBuilder = new AlipayTradeAppPayContentBuilder();
        $payContentBuilder->setSubject($payload['subject']);
        $payContentBuilder->setOutTradeNo($payload['out_trade_no']);
        $payContentBuilder->setTotalAmount($payload['total_amount']);
        if (isset($payload['body'])) {
            $payContentBuilder->setBody($payload['body']);
        }
        $payContentBuilder->setProductCode('QUICK_MSECURITY_PAY');
        $request = new AppRequest ();
        if (isset($payload['notify_url'])) {
            $request->setNotifyUrl($payload['notify_url']);
        }
        if (isset($payload['return_url'])) {
            $request->setReturnUrl($payload['return_url']);
        }
        $request->setBizContent($payContentBuilder->getBizContent());
        $request->setApiMethodName("alipay.trade.app.pay");
        return ($this->sdkExecute($request));
    }

    /**
     * 小程序支付
     * @param array $payload
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function mini(array $payload)
    {
        $payContentBuilder = new AlipayTradeCreateContentBuilder();
        $payContentBuilder->setSubject($payload['subject']);
        $payContentBuilder->setOutTradeNo($payload['out_trade_no']);
        $payContentBuilder->setTotalAmount($payload['total_amount']);
        $payContentBuilder->setBuyerId($payload['buyer_id']);
        if (isset($payload['body'])) {
            $payContentBuilder->setBody($payload['body']);
        }
        $payContentBuilder->setProductCode('FACE_TO_FACE_PAYMENT');
        $request = new AppRequest ();
        if (isset($payload['notify_url'])) {
            $request->setNotifyUrl($payload['notify_url']);
        }
        if (isset($payload['return_url'])) {
            $request->setReturnUrl($payload['return_url']);
        }
        $request->setBizContent($payContentBuilder->getBizContent());
        $request->setApiMethodName("alipay.trade.create");
        return ($this->execute($request));
    }

    /**
     * 扫码支付
     * @param array $payload
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function scan(array $payload)
    {
        $payContentBuilder = new AlipayTradePrecreateContentBuilder();
        $payContentBuilder->setSubject($payload['subject']);
        $payContentBuilder->setOutTradeNo($payload['out_trade_no']);
        $payContentBuilder->setTotalAmount($payload['total_amount']);
        if (isset($payload['body'])) {
            $payContentBuilder->setBody($payload['body']);
        }
        $payContentBuilder->setProductCode('FACE_TO_FACE_PAYMENT');
        $request = new AppRequest ();
        if (isset($payload['notify_url'])) {
            $request->setNotifyUrl($payload['notify_url']);
        }
        if (isset($payload['return_url'])) {
            $request->setReturnUrl($payload['return_url']);
        }
        $request->setBizContent($payContentBuilder->getBizContent());
        $request->setApiMethodName("alipay.trade.precreate");
        return ($this->execute($request));
    }

    /**
     * wap 支付
     * @param array $payload
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function wap(array $payload)
    {
        $payContentBuilder = new AlipayTradeWapPayContentBuilder();
        $payContentBuilder->setSubject($payload['subject']);
        $payContentBuilder->setOutTradeNo($payload['out_trade_no']);
        $payContentBuilder->setTotalAmount($payload['total_amount']);
        if (isset($payload['body'])) {
            $payContentBuilder->setBody($payload['body']);
        }
        $payContentBuilder->setProductCode('QUICK_WAP_WAY');
        $request = new AppRequest ();
        if (isset($payload['notify_url'])) {
            $request->setNotifyUrl($payload['notify_url']);
        }
        if (isset($payload['return_url'])) {
            $request->setReturnUrl($payload['return_url']);
        }
        $request->setBizContent($payContentBuilder->getBizContent());
        $request->setApiMethodName("alipay.trade.wap.pay");
        return ($this->execute($request));
    }

    /**
     * web 支付
     * @param array $payload
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function web(array $payload)
    {
        $payContentBuilder = new AlipayTradeWapPayContentBuilder();
        $payContentBuilder->setSubject($payload['subject']);
        $payContentBuilder->setOutTradeNo($payload['out_trade_no']);
        $payContentBuilder->setTotalAmount($payload['total_amount']);
        if (isset($payload['body'])) {
            $payContentBuilder->setBody($payload['body']);
        }
        $payContentBuilder->setProductCode('FAST_INSTANT_TRADE_PAY');
        $request = new AppRequest ();
        if (isset($payload['notify_url'])) {
            $request->setNotifyUrl($payload['notify_url']);
        }
        if (isset($payload['return_url'])) {
            $request->setReturnUrl($payload['return_url']);
        }
        $request->setBizContent($payContentBuilder->getBizContent());
        $request->setApiMethodName("alipay.trade.page.pay");
        return ($this->execute($request));
    }
}
