<?php

namespace EasyAlipay\Payment;

use Closure;
use EasyAlipay\Kernel\ServiceContainer;

/**
 * Class Application.
 *
 * @property \EasyAlipay\Payment\Cancel\Client            $cancel
 * @property \EasyAlipay\Payment\Close\Client             $close
 * @property \EasyAlipay\Payment\Pay\Client               $pay
 * @property \EasyAlipay\Payment\Query\Client             $query
 * @property \EasyAlipay\Payment\Refund\Client            $refund
 * @property \EasyAlipay\Payment\Fund\Client              $fund
 */

class Application extends ServiceContainer
{
    /**
     * @var array
     */
    protected $providers = [
        Cancel\ServiceProvider::class,
        Close\ServiceProvider::class,
        Pay\ServiceProvider::class,
        Query\ServiceProvider::class,
        Refund\ServiceProvider::class,
        Fund\ServiceProvider::class
    ];

    /**
     * 支付回调
     * @param Closure $closure
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \EasyAlipay\Kernel\Exceptions\Exception
     */
    public function handlePaidNotify(Closure $closure)
    {
        return (new Notify\Paid($this))->handle($closure);
    }
}
