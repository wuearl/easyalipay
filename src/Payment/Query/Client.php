<?php

namespace EasyAlipay\Payment\Query;

use EasyAlipay\Kernel\AppClient;
use EasyAlipay\Kernel\AppRequest;
use EasyAlipay\Payment\Model\AlipayTradeQueryContentBuilder;

class Client extends AppClient
{
    /**
     * 查询订单
     * @param string $out_trade_no
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function query(string $out_trade_no)
    {
        $queryContentBuilder = new AlipayTradeQueryContentBuilder();
        $queryContentBuilder->setOutTradeNo($out_trade_no);
        $request = new AppRequest ();
        $request->setBizContent($queryContentBuilder->getBizContent());
        $request->setApiMethodName("alipay.trade.query");
        return($this->execute($request)) ;
    }
}


