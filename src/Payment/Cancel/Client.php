<?php

namespace EasyAlipay\Payment\Cancel;

use EasyAlipay\Kernel\AppClient;
use EasyAlipay\Kernel\AppRequest;
use EasyAlipay\Payment\Model\AlipayTradeCancelContentBuilder;

class Client extends AppClient
{
    /**
     * 取消订单
     * @param string $out_trade_no
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function cancel(string $out_trade_no)
    {
        //构造查询业务请求参数对象
        $cancelContentBuilder = new AlipayTradeCancelContentBuilder();
        $cancelContentBuilder->setOutTradeNo($out_trade_no);
        $request = new AppRequest ();
        $request->setBizContent($cancelContentBuilder->getBizContent());
        $request->setApiMethodName("alipay.trade.cancel");
        return($this->execute($request)) ;
    }
}
