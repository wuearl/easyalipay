<?php

namespace EasyAlipay\Payment\Refund;

use EasyAlipay\Kernel\AppClient;
use EasyAlipay\Kernel\AppRequest;
use EasyAlipay\Payment\Model\AlipayTradeRefundContentBuilder;

class Client extends AppClient
{
    /**
     * @param string $out_trade_no
     * @param string $refund_amount
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function refund(string $out_trade_no, string $refund_amount, string $refund_reason = '订单退款', string $out_request_no = '')
    {
        //构造查询业务请求参数对象
        $refundContentBuilder = new AlipayTradeRefundContentBuilder();
        $refundContentBuilder->setOutTradeNo($out_trade_no);
        $refundContentBuilder->setRefundAmount($refund_amount);
        $refundContentBuilder->setRefundReason($refund_reason);
        if ($out_request_no) {
            $refundContentBuilder->setOutRequestNo($out_request_no);
        }
        $request = new AppRequest ();
        $request->setBizContent($refundContentBuilder->getBizContent());
        $request->setApiMethodName("alipay.trade.refund");
        return ($this->execute($request));
    }
}
