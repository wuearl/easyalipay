<?php

namespace EasyAlipay\Payment\Fund;

use EasyAlipay\Kernel\AppClient;
use EasyAlipay\Kernel\AppRequest;
use EasyAlipay\Payment\Model\AlipayFundTransUniTransferContentBuilder;

class Client extends AppClient
{
    /**
     * @param array $payload
     * @return \EasyAlipay\Kernel\Support\Collection
     * @throws \EasyAlipay\Kernel\Exceptions\BadRequestException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidArgumentException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidSignException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function transfer(array $payload)
    {
        //构造查询业务请求参数对象
        $contentBuilder = new AlipayFundTransUniTransferContentBuilder();
        $contentBuilder->setOutBizNo($payload['out_biz_no']);
        $contentBuilder->setTransAmount($payload['trans_amount']);
        $contentBuilder->setIdentity($payload['identity']);
        $productCode = $payload['product_code'] ?? 'TRANS_ACCOUNT_NO_PWD';
        $contentBuilder->setProductCode($productCode);
        if (isset($payload['identity_type'])) {
            $contentBuilder->setIdentityType($payload['identity_type']);
        }
        if (isset($payload['name'])) {
            $contentBuilder->setIdentityName($payload['name']);
        }
        if (isset($payload['remark'])) {
            $contentBuilder->setRemark($payload['remark']);
        }
        $bizScene = $payload['biz_scene'] ?? 'DIRECT_TRANSFER';
        $contentBuilder->setBizScene($bizScene);
        if (isset($payload['order_title'])) {
            $contentBuilder->setOrderTitle($payload['order_title']);
        }
        if (isset($payload['original_order_id'])) {
            $contentBuilder->setOriginalOrderId($payload['original_order_id']);
        }
        if (isset($payload['business_params'])) {
            $contentBuilder->setBusinessParams($payload['business_params']);
        }
        $request = new AppRequest ();
        $request->setBizContent($contentBuilder->getBizContent());
        $request->setApiMethodName("alipay.fund.trans.uni.transfer");
        return ($this->execute($request));
    }
}
