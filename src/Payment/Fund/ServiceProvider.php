<?php

namespace EasyAlipay\Payment\Fund;

use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ServiceProvider implements ServiceProviderInterface
{
    public function register(Container $app)
    {
        $app['fund'] = function ($app) {
            return new Client($app);
        };
    }
}
