<?php

/*
 * This file is part of the overtrue/wechat.
 *
 * (c) overtrue <i@overtrue.me>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace EasyAlipay\Payment\Notify;

use Closure;
use EasyAlipay\Kernel\Exceptions\Exception;
use EasyAlipay\Kernel\Exceptions\InvalidSignException;
use EasyAlipay\Kernel\Traits\SingData;
use Symfony\Component\HttpFoundation\Response;

abstract class Handler
{
    use SingData;

    const SUCCESS = 'success';
    const FAIL = 'fail';

    /**
     * @var \EasyAlipay\Payment\Application
     */
    protected $app;

    /**
     * @var array
     */
    protected $message;

    /**
     * @var string|null
     */
    protected $fail;

    /**
     * @var array
     */
    protected $attributes = [];

    /**
     * Check sign.
     * If failed, throws an exception.
     *
     * @var bool
     */
    protected $check = true;

    /**
     * Respond with sign.
     *
     * @var bool
     */
    protected $sign = false;

    /**
     * @param \EasyAlipay\Payment\Application $app
     */
    public function __construct($app)
    {
        $this->app = $app;
    }

    /**
     * Handle incoming notify.
     *
     * @param \Closure $closure
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    abstract public function handle(Closure $closure);

    /**
     * @param string $message
     */
    public function fail(string $message)
    {
        $this->fail = $message;
    }

    /**
     * @param array $attributes
     * @param bool $sign
     *
     * @return $this
     */
    public function respondWith(array $attributes, bool $sign = false)
    {
        $this->attributes = $attributes;
        $this->sign = $sign;

        return $this;
    }

    /**
     * return the response to ali.
     *
     * @return Response
     */
    public function toResponse(): Response
    {
        return new Response($this->fail);
    }

    /**
     * Return the notify message from request.
     *
     * @return array
     *
     * @throws \EasyAlipay\Kernel\Exceptions\Exception
     */
    public function getMessage(): array
    {
        if (!empty($this->message)) {
            return $this->message;
        }

        try {
            parse_str($this->app['request']->getContent(), $message);
        } catch (\Throwable $e) {
            throw new Exception('Invalid request: ' . $e->getMessage(), 400);
        }

        if (!is_array($message) || empty($message)) {
            throw new Exception('Invalid request.', 400);
        }

        if ($this->check) {
            $this->validate($message);
        }

        return $this->message = $message;
    }

    /**
     * Validate the request params.
     * @param array $message
     * @throws InvalidSignException
     * @throws \EasyAlipay\Kernel\Exceptions\InvalidConfigException
     */
    protected function validate(array $message)
    {
        $sign = $message['sign'];
        $sign_type = $message['sign_type'];
        unset($message['sign'], $message['sign_type']);

        if ($this->verify($message, $sign, $this->app['config']->get('alipay_public_key'), $sign_type)) {
            throw new InvalidSignException();
        }
    }

    /**
     * @param mixed $result
     */
    protected function strict($result)
    {
        if (true !== $result && is_null($this->fail)) {
            $this->fail(strval($result));
        }
    }
}
