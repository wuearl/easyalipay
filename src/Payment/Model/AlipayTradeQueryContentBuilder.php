<?php

namespace EasyAlipay\Payment\Model;

/**
 * 功能：alipay.trade.query(统一收单线下交易查询)接口业务参数封装
 */
class AlipayTradeQueryContentBuilder extends BaseContentBuilder
{
    /**
     * 支付宝交易号
     * @var string
     */
    protected $tradeNo;

    /**
     * 银行间联模式下有用，其它场景请不要使用,双联通过该参数指定需要查询的交易所属收单机构的pid;
     * @var string
     */
    protected $orgPid;

    /**
     * 获取支付宝交易号
     * @return string
     */
    public function getTradeNo()
    {
        return $this->tradeNo;
    }

    /**
     * 设置支付宝交易号
     * @param $tradeNo
     */
    public function setTradeNo($tradeNo)
    {
        $this->tradeNo = $tradeNo;
        $this->bizContentarr['trade_no'] = $tradeNo;
    }

    /**
     * 获取机构pid
     * @return string
     */
    public function getOrgPid()
    {
        return $this->orgPid;
    }

    /**
     * 设置机构pid
     * @param $orgPid
     */
    public function setOrgPid($orgPid)
    {
        $this->orgPid = $orgPid;
        $this->bizContentarr['org_pid'] = $orgPid;
    }
}