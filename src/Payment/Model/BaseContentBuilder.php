<?php
/**
 * Created by PhpStorm.
 * User: wuliangbo
 * Date: 2020/1/7
 * Time: 15:24
 */

namespace EasyAlipay\Payment\Model;
class BaseContentBuilder
{
    /**
     * 商户订单号.
     * @var string
     */
    protected $outTradeNo;

    /**
     * 订单标题，粗略描述用户的支付目的。
     * @var string
     */
    protected $subject;

    /**
     * 订单总金额，整形，此处单位为元，精确到小数点后2位，不能超过1亿元
     * @var float
     */
    protected $totalAmount;

    /**
     * 请求参数数组
     * @var array
     */
    protected $bizContentarr = array();

    /**
     * 请求参数字符串
     * @var null|string
     */
    protected $bizContent = NULL;

    /**
     * 订单具体描述
     * @var string
     */
    protected $body = '';

    /**
     * 销售产品码
     * @var string
     */
    protected $productCode = '';
    
    /**
     * 获取参数集合
     * @return false|string|null
     */
    public function getBizContent()
    {
        if (!empty($this->bizContentarr)) {
            $this->bizContent = json_encode($this->bizContentarr, JSON_UNESCAPED_UNICODE);
        }
        return $this->bizContent;
    }

    /**
     * 设置订单号
     * @param $outTradeNo
     */
    public function setOutTradeNo($outTradeNo)
    {
        $this->outTradeNo = $outTradeNo;
        $this->bizContentarr['out_trade_no'] = $outTradeNo;
    }

    /**
     * 设置订单标题
     * @param $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
        $this->bizContentarr['subject'] = $subject;
    }

    /**
     * 获取订单标题
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * 设置订单金额
     * @param $totalAmount
     */
    public function setTotalAmount($totalAmount)
    {
        $this->totalAmount = $totalAmount;
        $this->bizContentarr['total_amount'] = $totalAmount;
    }

    /**
     * 获取订单金额
     * @return float
     */
    public function getTotalAmount()
    {
        return $this->totalAmount;
    }

    /**
     * 获取订单具体描述
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * 设置订单具体描述
     * @param $body
     */
    public function setBody($body)
    {
        $this->body = $body;
        $this->bizContentarr['body'] = $body;
    }

    /**
     * 设置销售产品码
     * @param $productCode
     */
    public function setProductCode($productCode)
    {
        $this->productCode = $productCode;
        $this->bizContentarr['product_code'] = $productCode;
    }

    /**
     * 获取销售产品码
     * @return string
     */
    public function getProductCode()
    {
        return $this->productCode;
    }
}