<?php
/**
 * Created by PhpStorm.
 * User: wuliangbo
 * Date: 2020/1/7
 * Time: 15:19
 */

namespace EasyAlipay\Payment\Model;
class AlipayTradePagePayContentBuilder extends BaseContentBuilder
{
    /**
     * PC扫码支付的方式 0:订单码-简约前置模式|1:订单码-前置模式|3:订单码-迷你前置模式|4:订单码-可定义宽度的嵌入式二维码
     * @var string
     */
    protected $qrPayMode;

    /**
     *商户自定义二维码宽度
     * @var int
     */
    protected $qrcodeWidth;

    /**
     * 设置pc扫码方式
     * @param $qrPayMode
     */
    public function setQrPayMode($qrPayMode)
    {
       $this->qrPayMode = $qrPayMode;
       $this->bizContentarr['qr_pay_mode'] = $qrPayMode;
    }

    /**
     * 获取pc扫码方式
     * @return string
     */
    public function getQrPayMode()
    {
        return $this->qrPayMode;
    }

    /**
     * 设置二维码宽度
     * @param $qrcodeWidth
     */
    public function setQrcodeWidth($qrcodeWidth)
    {
        $this->qrcodeWidth = $qrcodeWidth;
        $this->bizContentarr['qrcode_width'] = $qrcodeWidth;

    }

    /**
     * 获取二维码宽度
     * @return int
     */
    public function getQrcodeWidth()
    {
        return $this->qrcodeWidth;
    }
}