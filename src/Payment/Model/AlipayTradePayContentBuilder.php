<?php

namespace EasyAlipay\Payment\Model;

/* *
 * 功能：alipay.trade.pay(统一收单交易支付接口)接口业务参数封装
 */

class AlipayTradePayContentBuilder extends BaseContentBuilder
{
    /**
     * 支付场景
     * @var string
     */
    protected $scene;

    /**
     * 支付授权码
     * @var string
     */
    protected $authCode;

    /**
     * 获取支付场景
     * @return string
     */
    public function getScene()
    {
        return $this->scene;
    }

    /**
     * 设置支付场景
     * @param $scene
     */
    public function setScene($scene)
    {
        $this->scene = $scene;
        $this->bizContentarr['scene'] = $scene;
    }

    /**
     * 获取支付授权码
     * @return string
     */
    public function getAuthCode()
    {
        return $this->authCode;
    }

    /**
     * 设置支付授权码
     * @param $authCode
     */
    public function setAuthCode($authCode)
    {
        $this->authCode = $authCode;
        $this->bizContentarr['authCode'] = $authCode;
    }
}