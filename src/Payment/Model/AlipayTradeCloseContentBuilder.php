<?php

namespace EasyAlipay\Payment\Model;

/**
 * 功能：alipay.trade.close (统一收单交易关闭接口)业务参数封装
 */

class AlipayTradeCloseContentBuilder extends BaseContentBuilder
{
    /**
     * 支付宝交易号
     * @var string
     */
    protected $tradeNo;

    /**
     * 卖家端自定义的的操作员 ID
     * @var string
     */
    protected $operatorId;

    /**
     * 获取支付宝交易号
     * @return string
     */
    public function getTradeNo()
    {
        return $this->tradeNo;
    }

    /**
     * 设置支付宝交易号
     * @param $tradeNo
     */
    public function setTradeNo($tradeNo)
    {
        $this->tradeNo = $tradeNo;
        $this->bizContentarr['trade_no'] = $tradeNo;
    }

    /**
     * 获取卖家端自定义的的操作员 ID
     * @return string
     */
    public function getOperatorId()
    {
        return $this->operatorId;
    }

    /**
     * 设置卖家端自定义的的操作员 ID
     * @param $operatorId
     */
    public function setOperatorId($operatorId)
    {
        $this->operatorId = $operatorId;
        $this->bizContentarr['operator_id'] = $operatorId;
    }
}

?>