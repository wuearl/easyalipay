<?php

namespace EasyAlipay\Payment\Model;

/**
 * 功能：alipay.trade.create (统一收单交易关闭接口)业务参数封装
 */

class AlipayTradeCreateContentBuilder extends BaseContentBuilder
{
    /**
     * 买家的支付宝唯一用户号
     * @var string
     */
    protected $buyerId;

    /**
     * 获取买家的支付宝唯一用户号
     * @return string
     */
    public function getBuyerId()
    {
    	return $this->buyerId;
    }

    /**
     * 设置买家的支付宝唯一用户号
     * @param $buyerId
     */
    public function setBuyerId($buyerId)
    {
    	$this->buyerId = $buyerId;
    	$this->bizContentarr['buyer_id'] = $buyerId;
    }
}