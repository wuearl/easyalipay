<?php
/**
 * Created by PhpStorm.
 * User: wuliangbo
 * Date: 2020/3/6
 * Time: 15:32
 */

namespace EasyAlipay\Payment\Model;
class AlipayFundTransUniTransferContentBuilder extends BaseContentBuilder
{
    /**
     * 唯一订单号
     * @var string
     */
    protected $outBizNo;
    /**
     * 转账金额
     * @var string
     */
    protected $transAmount;
    /**
     * 业务产品码 STD_RED_PACKET|TRANS_ACCOUNT_NO_PWD|TRANS_BANKCARD_NO_PWD
     * @var string
     */
    protected $productCode = 'TRANS_ACCOUNT_NO_PWD';
    /**
     * 特定场景描述 PERSONAL_COLLECTION|DIRECT_TRANSFER
     * @var string
     */
    protected $bizScene;
    /**
     * 原支付宝业务单号  C2C现金红包-红包领取时，传红包支付时返回的支付宝单号
     * @var string
     */
    protected $originalOrderId;
    /**
     * 收款方信息
     * @var array
     */
    protected $payeeInfo = [
        'identity' => '',
        'identity_type' => 'ALIPAY_USER_ID',
        'name' => ''
    ];
    /**
     * 业务备注
     * @var string
     */
    protected $remark;
    /**
     * 转账标题
     * @var string
     */
    protected $orderTitle;
    /**
     * 转账业务请求的扩展参数
     * @var string
     */
    protected $businessParams;

    /**
     * 获取唯一订单号
     * @return string
     */
    public function getOutBizNo()
    {
        return $this->outBizNo;
    }

    /**
     * 设置唯一订单号
     * @param $outBizNo
     */
    public function setOutBizNo($outBizNo)
    {
        $this->outBizNo = $outBizNo;
        $this->bizContentarr['out_biz_no'] = $outBizNo;
    }

    public function getTransAmount()
    {
        return $this->transAmount;
    }

    public function setTransAmount($transAmount)
    {
        $this->transAmount = $transAmount;
        $this->bizContentarr['trans_amount'] = $transAmount;
    }

    public function getBizScene()
    {
        return $this->bizScene;
    }

    public function setBizScene($bizScene)
    {
        $this->bizScene = $bizScene;
        $this->bizContentarr['biz_scene'] = $bizScene;
    }

    public function getOrderTitle()
    {
        return $this->orderTitle;
    }

    public function setOrderTitle($orderTitle)
    {
        $this->orderTitle = $orderTitle;
        $this->bizContentarr['order_title'] = $orderTitle;
    }

    public function getOriginalOrderId()
    {
        return $this->originalOrderId;
    }

    public function setOriginalOrderId($originalOrderId)
    {
        $this->originalOrderId = $originalOrderId;
        $this->bizContentarr['original_order_id'] = $originalOrderId;
    }

    public function setIdentity($identity)
    {
        $this->payeeInfo['identity'] = $identity;
        $this->bizContentarr['payee_info'] = $this->payeeInfo;
    }

    public function setIdentityType($identityType)
    {
        $this->payeeInfo['identity_type'] = $identityType;
        $this->bizContentarr['payee_info'] = $this->payeeInfo;
    }

    public function setIdentityName($name)
    {
        $this->payeeInfo['name'] = $name;
        $this->bizContentarr['payee_info'] = $this->payeeInfo;
    }

    public function getRemark()
    {
        return $this->remark;
    }

    public function setRemark($remark)
    {
        $this->remark = $remark;
        $this->bizContentarr['remark'] = $remark;
    }

    public function getBusinessParams()
    {
        return $this->remark;
    }

    public function setBusinessParams($businessParams)
    {
        $this->businessParams = $businessParams;
        $this->bizContentarr['business_params'] = $businessParams;
    }

    public function setProductCode($productCode = 'TRANS_ACCOUNT_NO_PWD')
    {
        $this->productCode = $productCode;
        $this->bizContentarr['product_code'] = $productCode;
    }
}