<?php

namespace EasyAlipay\Payment\Model;

/**
 * 功能：alipay.trade.refund(统一收单交易退款接口)接口业务参数封装
 */
class AlipayTradeRefundContentBuilder extends BaseContentBuilder
{
    /**
     * 支付宝交易号
     * @var
     */
    protected $tradeNo;

    /**
     * 退款金额
     * @var float
     */
    protected $refundAmount;

    /**
     * 退款标识
     * @var string
     */
    protected $outRequestNo;

    /**
     * 退分账明细信息
     * @var array
     */
    protected $refundRoyaltyParameters = [];

    /**
     * 退款币种信息
     * @var string
     */
    protected $refundCurrency;

    /**
     * 退款原因
     * @var string
     */
    protected $refundReason;

    /**
     * 获取支付宝交易号
     * @return mixed
     */
    public function getTradeNo()
    {
        return $this->tradeNo;
    }

    /**
     * 设置支付宝交易号
     * @param $tradeNo
     */
    public function setTradeNo($tradeNo)
    {
        $this->tradeNo = $tradeNo;
        $this->bizContentarr['trade_no'] = $tradeNo;
    }

    /**
     * 获取退款金额
     * @return float
     */
    public function getRefundAmount()
    {
        return $this->refundAmount;
    }

    /**
     * 设置退款金额
     * @param $refundAmount
     */
    public function setRefundAmount($refundAmount)
    {
        $this->refundAmount = $refundAmount;
        $this->bizContentarr['refund_amount'] = $refundAmount;
    }

    /**
     * 获取退款标识
     * @return string
     */
    public function getOutRequestNo()
    {
        return $this->outRequestNo;
    }

    /**
     * 设置退款标识
     * @param $outRequestNo
     */
    public function setOutRequestNo($outRequestNo)
    {
        $this->outRequestNo = $outRequestNo;
        $this->bizContentarr['out_request_no'] = $outRequestNo;
    }

    /**
     * 获取退款币种
     * @return string
     */
    public function getRefundCurrency()
    {
        return $this->refundCurrency;
    }

    /**
     * 设置退款币种
     * @param $refundCurrency
     */
    public function setRefundCurrency($refundCurrency)
    {
        $this->refundCurrency = $refundCurrency;
        $this->bizContentarr['refund_currency'] = $refundCurrency;
    }

    /**
     * 获取退款原因
     * @return string
     */
    public function getRefundReason()
    {
        return $this->refundReason;
    }

    /**
     * 设置退款原因
     * @param $refundReason
     */
    public function setRefundReason($refundReason)
    {
        $this->refundReason = $refundReason;
        $this->bizContentarr['refund_reason'] = $refundReason;
    }

    /**
     * 获取退分账明细信息
     * @return array
     */
    public function getRefundRoyaltyParameters()
    {
        return $this->refundRoyaltyParameters;
    }

    /**
     * 设置退分账明细信息
     * @param $refundRoyaltyParameters
     */
    public function setRefundRoyaltyParameters($refundRoyaltyParameters)
    {
        $this->refundRoyaltyParameters = $refundRoyaltyParameters;
        $this->bizContentarr['refund_royalty_parameters'] = $refundRoyaltyParameters;
    }
}