<?php

namespace EasyAlipay\Payment\Model;
/**
 * 功能：alipay.trade.cancel (统一收单交易关闭接口)业务参数封装
 */

class AlipayTradeCancelContentBuilder extends BaseContentBuilder
{
    /**
     * 支付宝交易号
     * @var string
     */
    protected $tradeNo;

    /**
     * 获取支付宝交易号
     * @return string
     */
    public function getTradeNo()
    {
        return $this->tradeNo;
    }

    /**
     * 设置支付宝交易号
     * @param $tradeNo
     */
    public function setTradeNo($tradeNo)
    {
        $this->tradeNo = $tradeNo;
        $this->bizContentarr['trade_no'] = $tradeNo;
    }
}